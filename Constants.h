//
//  Constants.h
//  friendsquare
//
//  Created by magnon on 23/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <Foundation/Foundation.h>
//#define kBaseUrl @"http://php.delivery-projects.com:81/friendzsquare/api/index/login"
#define kBaseUrl @"http://magnon.co.in/api/index/login"
#define kBaseUrlPostUpload  @"http://magnon.co.in/api/index/wallpost"
#define kBaseUrlPostList @"http://magnon.co.in/api/index/wallpostlist"
#define KBaseUrlPostListImages @"http://magnon.co.in/uploads/wall_image/"

#define kBaseUrlSocial @"http://magnon.co.in/api/index/sociallogin"
#define kBaseUrlRegister @"http://magnon.co.in/api/index/register"
#define kBaseUrlForgot @"http://magnon.co.in/api/index/forgotpassword"
#define kBaseUrlSearchFriend @"http://magnon.co.in/api/index/searchuser"
#define kBaseUrlGetUserProfile @"http://magnon.co.in/api/index/getuserprofile"
#define kBaseUrlLikePost @"http://magnon.co.in/api/index/walllike"
#define kBaseUrlCommentList @"http://magnon.co.in/api/index/commentlist"
#define kBaseUrlpostComment @"http://magnon.co.in/api/index/commentpost/"
#define kBaserUrlgetFriendList @"http://magnon.co.in/api/index/getuserconnectionlist"
#define CONTENT_TYPE_HTML       [NSSet setWithObject:@"text/html"]
#define MainStoryBoard ([UIStoryboard storyboardWithName:@"Main" bundle: nil])
#define App_title @"Friendz Square"
#define GET_USERID     [[NSUserDefaults standardUserDefaults] valueForKey:@"u_id"]

#define  kBaseUrlChangeConnectionStatus @"http://magnon.co.in/api/index/changeconnectionstatus"

#define kBaseUrlPeopleyoumayknow @"http://magnon.co.in/api/index/getprofileyoumayknowlist"

#define KBaseUrlCommetchat @"http://magnon.co.in/public/cometchat"
#define kBaseUrlFriendRequest @"http://magnon.co.in/api/index/getuserconnectionrequestlist"






//User credentials

#define FSDisplayname             @"u_displayname"
#define FSUserid                  @"u_id"
#define FSfirstname             @"u_first_name"
#define FSlastname             @"u_last_name"
#define FSemail           @"u_email"
#define FSgender            @"u_gender"
#define FSpersonalVisibility             @"u_personal_visibility"
#define FSmaritalstatus             @"u_marital_status"
#define FSprofilePhoto             @"u_profile_photo"
#define FScountry             @"u_country"
#define FSState             @"u_state"
#define FSCity             @"u_city"
#define FSEducationLevel             @"u_education_level"
#define FSCollegeSpecialization             @"u_college_specialization"
#define FSGraduateYear             @"u_graduate_year"
#define FSStudyStatus             @"u_study_status"
#define FSCityEstablishment             @"u_city_establishment"
#define FSCountryEstablishment             @"u_country_establishment"
#define FSEducationEstablishment             @"u_education_establishment"
#define FSProfessionalVisibility             @"u_professional_visibility"
#define FSCurrentProfession             @"u_current_profession"
#define FSSubProfession             @"u_sub_profession"
#define FSCurrentPosition             @"u_current_position"
#define FSUDoc             @"u_doc"
#define FSZipCode             @"u_zip_code"
#define FSPhoneNumber             @"u_phone_number"
#define FSQualificationVisibility             @"u_qualification_visibility"

#define FSTotalConnection @"total_connection"





@interface Constants : NSObject

@end
