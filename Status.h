//
//  Status.h
//  friendsquare
//
//  Created by magnon on 26/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
@interface Status : JSONModel{
    
}
@property(nonatomic,copy)NSString *error;
@property(nonatomic,copy)NSString *message;
@end
