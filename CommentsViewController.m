//
//  CommentsViewController.m
//  friendsquare
//
//  Created by magnon on 02/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "CommentsViewController.h"
#import "BounceAnimationController.h"
#import "RoundRectPresentationController.h"
#import "CommentTableViewCell.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "Status.h"
#import "Utils.h"
#import "Base64.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "NSDate+Additions.h"
@interface CommentsViewController ()<UIViewControllerTransitioningDelegate>
@property(strong,nonatomic)UIRefreshControl *refreshControl;
@end
static NSString* const CellIdentifier = @"CommentTableViewCell";
@implementation CommentsViewController
@synthesize commentListArray;

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        if ([self respondsToSelector:@selector(setTransitioningDelegate:)]) {
            self.modalPresentationStyle = UIModalPresentationCustom;
            self.transitioningDelegate = self;
        }
    }
    return self;
}
- (void)viewDidLoad {
    commentListArray=[[NSMutableArray alloc]init];
    NSLog(@"Wall id is %@",_wallId);
    _addcommentTextview.text=@"Add Comment.";
    // Initialize the refresh control.
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(getCommentList)
                  forControlEvents:UIControlEventValueChanged];
    [_commentTbl addSubview:self.refreshControl];
 
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [self getCommentList];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)getCommentList{
   // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //  {"wallid":"7"}
   
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
   
    [postDict setValue:[NSString stringWithFormat:@"%@",_wallId] forKey:@"wallid"];
   
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlCommentList parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"comment list is %@",responseObject);
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
           // [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Utils showAlertwithMessage:status.message];
        }
        else{
            
            /*comment = "Good morning";
             "comment_id" = 38;
             created = "2015-12-07 03:58:33";
             "u_first_name" = RAM;
             "u_last_name" = PAL;
             "u_profile_photo" = "http://magnon.co.in/uploads/profile_photos/14491444579b88c920ce123dfaa9270151873e1a68.jpg";
             userid = 125;
             "wall_id" = 107;*/
            _comments=[[CommentList alloc]initWithDictionary:responseObject error:&err];
            
            
            [self.refreshControl endRefreshing];

            
            // NSLog(@"JSON: %@", _feed.feed);
            [self performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
            //[Utils showAlertwithMessage:postListArray];
           // [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        }
    }
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
             // [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
    
    
}
- (void)reloadData
{
    // Reload table data
    [_commentTbl reloadData];
    // End the refreshing
    if (self.refreshControl) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMM d, h:mm a"];
        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
                                                                    forKey:NSForegroundColorAttributeName];
        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
        self.refreshControl.attributedTitle = attributedTitle;
        
        [self.refreshControl endRefreshing];
    }
    
}

# pragma mark - Cell Setup

- (void)setUpCell:(CommentTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    CommentModel *commentCell = _comments.comments[indexPath.row];
    
    NSLog(@"Comment array is %@",commentCell);
    /*comment = "Good morning";
     "comment_id" = 38;
     created = "2015-12-07 03:58:33";
     "u_first_name" = RAM;
     "u_last_name" = PAL;
     "u_profile_photo" = "http://magnon.co.in/uploads/profile_photos/14491444579b88c920ce123dfaa9270151873e1a68.jpg";
     userid = 125;
     "wall_id" = 107;*/
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    });
    NSDate *date = [dateFormatter dateFromString:commentCell.created];
    
    
    cell.nameLbl.text=[Utils fullName:commentCell.u_first_name :commentCell.u_last_name];//[post valueForKey:@"u_first_name"];
    cell.datetimeLbl.text=[NSString stringWithFormat:@"%@ %@", [date longRelativeDateString], [date timeString]];//[post valueForKey:@"created"];
    cell.commenttextLbl.text= commentCell.comment;//[post valueForKey:@"content"];
    
    
    NSURL *url = [NSURL URLWithString:commentCell.u_profile_photo];
    
    [cell.picView sd_setImageWithURL:url
                    placeholderImage:[UIImage imageNamed:@"noimage"]];
    

}
# pragma mark - UITableViewControllerDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (_comments.comments) {
        
        tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        tableView.backgroundView=nil;
        return 1;
        
    } else {
        
        // Display a message when the table is empty
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"No data is currently available. Please pull down to refresh.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    
    return 0;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _comments.comments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    [self setUpCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static CommentTableViewCell *cell = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    });
    
    [self setUpCell:cell atIndexPath:indexPath];
    NSLog(@"height is %f",[self calculateHeightForConfiguredSizingCell:cell]);
    return [self calculateHeightForConfiguredSizingCell:cell];
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma TextView delegate methods
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}
-(BOOL)textView:(UITextView *)_textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
   
    return YES;
}



- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    textView.textColor=[UIColor blackColor];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    NSLog(@"textViewDidBeginEditing:");
    if ([textView.text isEqualToString:@"Add Comment."]) {
        textView.text=nil;
    }
    else{
        textView.textColor=[UIColor blackColor];
    }
    
    //textView.backgroundColor = [UIColor greenColor];
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    NSLog(@"textViewShouldEndEditing:");
    textView.backgroundColor = [UIColor whiteColor];
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"textViewDidEndEditing:");
    if([textView.text isEqualToString:@""]){
        textView.text=@"Add Comment.";
        textView.textColor=[UIColor grayColor];
    }
    else{
        textView.textColor=[UIColor blackColor];
    }
}

#pragma mark - UIViewControllerTransitioningDelegate

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return [[BounceAnimationController alloc] init];
}

- (UIPresentationController *)presentationControllerForPresentedViewController:(UIViewController *)presented presentingViewController:(UIViewController *)presenting sourceViewController:(UIViewController *)source {
    return [[RoundRectPresentationController alloc] initWithPresentedViewController:presented presentingViewController:presenting];
}
- (IBAction)closeTbl:(id)sender {
     [self.delegate sendChangedPaswdToServer:self];
}
- (IBAction)addComment:(id)sender {
    [self.view endEditing:YES];
  
    // {"userid":"125","wall_id":"7","comment":""}
    
    
    if([_addcommentTextview.text isEqualToString:@"Add Comment."] ){
        //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        return;
    }
    else{
        NSString *updateText;
        if([_addcommentTextview.text isEqualToString:@"Add Comment."]){
            //  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            updateText=@"";
            //return;
        }
        else{
            updateText=_addcommentTextview.text;
        }
        NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
        [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]     forKey:@"u_id"];
        [postDict setValue:_wallId forKey:@"wall_id"];
        [postDict setValue:updateText forKey:@"comment"];
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [manager POST:kBaseUrlpostComment parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSLog(@"comment list is %@",responseObject);
            NSError* err = nil;
            Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
            
            if([status.error isEqualToString:@"True"]){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [Utils showAlertwithMessage:status.message];
            }
            else{
                
                /*comment = "Good morning";
                 "comment_id" = 38;
                 created = "2015-12-07 03:58:33";
                 "u_first_name" = RAM;
                 "u_last_name" = PAL;
                 "u_profile_photo" = "http://magnon.co.in/uploads/profile_photos/14491444579b88c920ce123dfaa9270151873e1a68.jpg";
                 userid = 125;
                 "wall_id" = 107;*/
                
                NSLog(@"Original feeds: %@", _comments.comments);
                if([_comments.comments count]==0){
                     _comments=[[CommentList alloc]initWithDictionary:responseObject error:&err];
                }
                else{
                
                CommentList *_new_feed=[[CommentList alloc]initWithDictionary:responseObject error:&err];
                
                
                
                CommentModel *post = [_new_feed.comments objectAtIndex:0];
                [_comments.comments insertObject:post atIndex:0];
                NSLog(@"New Feed: %@", _comments.comments);
                }
                
                _addcommentTextview.text=nil;
                [_commentTbl reloadData];
                //[Utils showAlertwithMessage:postListArray];
                [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
                
            }
        }
         
         
         
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"fail");
                  [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
              }];
        
        
    }
}


@end
