-------------------------------------------------------------
CometChat-SDK Change Log
-------------------------------------------------------------
-------------------------------------------------------------
1.4.4
-------------------------------------------------------------

iOS
-------
- UI Bug fixes for SDK Test App
- Bug fixes in CometChatSDK.framework

-------------------------------------------------------------
1.4.3
-------------------------------------------------------------

Android
--------
- Login with userid function added for Cloud login

iOS
-------
- Login with userID function added for Cloud login

-------------------------------------------------------------
1.4.2
-------------------------------------------------------------

iOS
-------
- Minor bug fixes

-------------------------------------------------------------
1.4.1
-------------------------------------------------------------

Android
--------
- Invite user to Chatroom function modified
- Error codes revamped
- 'getPluginInfo()' function added
- Guest login response modified
- Simulate Capture Photo/Share Photo from gallery added for OneOnOne and Chatroom Chat in CometChat SDK Sample App
- Simulate Capture Video/Share Video from gallery added for OneOnOne and Chatroom Chat in CometChat SDK Sample App
- Simulate block/unblock user in CometChat SDK Sample App

iOS
-------
- Invite user to Chatroom function modified
- Error codes revamped
- 'getPluginInfo' function added
- Simulate Capture Photo/Share Photo from photo library added for OneOnOne and Chatroom Chat in CometChat SDK Sample App
- Simulate Capture Video/Share Video from photo library added for OneOnOne and Chatroom Chat in CometChat SDK Sample App
- Simulate block/unblock user in CometChat SDK Sample App

-------------------------------------------------------------
1.4.0
-------------------------------------------------------------

iOS
-------
- Guest login bug fixed
- Guest login response modified

-------------------------------------------------------------
1.3.9
-------------------------------------------------------------

Android
--------
- Revamped OneOnOne and Chatroom Chat layout in CometChat SDK Sample App
- Simulate imageSharing,changeStatus/statusMessage and setTranslation language for OneOnOne and Chatroom Chat in CometChat SDK Sample App
- Fixed logout bug fixes
- Fixed the bug for no gotOnlineList() invoked after block user

iOS
-------
- Revamped OneOnOne and Chatroom Chat layout in CometChat SDK Sample App
- Simulate imageSharing,changeStatus/statusMessage and setTranslation language for OneOnOne and Chatroom Chat in CometChat SDK Sample App

-------------------------------------------------------------
1.3.8
-------------------------------------------------------------

Android
--------
- Bug fixes

-------------------------------------------------------------
1.3.7
-------------------------------------------------------------

Android
--------
- Delete Chatroom functionality added
- 'subscribeToChatroom' method modified, 'onKicked()' and 'onBanned()' callback removed,'onActionMessageReceived()' callback added.
- Bug fixes

iOS
-------
- Delete Chatroom functionality added
- 'subscribeToChatroom' method modified, 'onKicked' and 'onBanned' callback blocks removed,'onActionMessageReceived' callback block added.
- Logs formatted.
-------------------------------------------------------------
1.3.6
-------------------------------------------------------------

Android
--------
- Translate Conversation for 'OneOnOne’ and 'Chatroom’
- Switch camera for Audio/Video Chat and Group Audio/Video Chat added

iOS
-------
- Translate Conversation for 'OneOnOne’ and 'Chatroom’
- Switch camera for Audio/Video Chat and Group Audio/Video Chat added

-------------------------------------------------------------
1.3.5
-------------------------------------------------------------

Android
--------
- CometChat Cloud support added
- Chatroom type enum added for createChatroom


iOS
-------
- CometChat Cloud support added
- Chatroom type enum added for createChatroom

-------------------------------------------------------------
1.3.0
-------------------------------------------------------------

Android
--------
- Revamp UI of Sample App
- 'isLoggedIn' function deprecated
- Bug fixes

iOS
-------
- Revamp UI of Sample App
- Removed login retry attempts
- 'isLoggedIn’ function deprecated
-------------------------------------------------------------
1.2.4
-------------------------------------------------------------

Android
--------
- Added dev mode support
- Bug fix for isCometChatInstalled

iOS
-------
- Added dev mode support

-------------------------------------------------------------
1.2.3
-------------------------------------------------------------

Android
--------
- Minor bug fix with group AV chat endcall
- Fetch app settings for each instance

iOS
-------
- Fetch app settings for each instance

-------------------------------------------------------------
1.2.2
-------------------------------------------------------------

iOS
-------
- Minor bug fix in Check cometchat installation method.

-------------------------------------------------------------
1.2.1
-------------------------------------------------------------

iOS
-------
- Handled datatype of "old" field in received message response
- Create chatroom response bug fixed
- WebRTC channel bug fixed

-------------------------------------------------------------
1.2.0
-------------------------------------------------------------

Android
--------
- Added function to check CometChat is installed on specified
URL

iOS
-------
- Added function to check CometChat is installed on specified
URL

-------------------------------------------------------------
1.1.9
-------------------------------------------------------------

Android
--------
- Critical group AV chat bug fix
- Better compatible with CometChat 5.5 for AV chat
- Fixed a bug due to which subscribe polling was throwing
exceptions
- Minor bug fixes

-------------------------------------------------------------
1.1.8
-------------------------------------------------------------

Android
--------
- Bug fixes

iOS
----
- Custom emoji message bug fixed.

-------------------------------------------------------------
1.1.7
-------------------------------------------------------------

Android and iOS
----------------
- Added ability to fetch chat history
- Moved the message type parameter into the message JSONObject.
It is now available under the "message_type" parameter.

-------------------------------------------------------------
1.1.6
-------------------------------------------------------------

Android and iOS
----------------
- Added AV chat to chatrooms
- Bug fixes and code optimization

-------------------------------------------------------------
1.1.5
-------------------------------------------------------------

Android and iOS
----------------
- Added setStatus and setStatusMessage feature

-------------------------------------------------------------
1.1.4
-------------------------------------------------------------

Android
--------
- Added noise cancellation in audio/video chat (Feature)
- Disabled message resend mechanism (Bug fix)

-------------------------------------------------------------
1.1.3
-------------------------------------------------------------

Android and iOS
----------------
- Added unique parameter for identifying each platform

-------------------------------------------------------------
1.1.2
-------------------------------------------------------------

Android
--------
- URL validation rectified (Bug fix)

-------------------------------------------------------------
1.1.1
-------------------------------------------------------------

Android and iOS
----------------
- Added login with username and password
- Added function to check the current version number
- Message type changed

-------------------------------------------------------------
1.1.0
-------------------------------------------------------------

Android and iOS
----------------
- Added Audio/Video calling

-------------------------------------------------------------
1.0.8
-------------------------------------------------------------

Android
--------
- Fixed bug with realtime announcement
- Fixed bug with image sharing

-------------------------------------------------------------
1.0.7
-------------------------------------------------------------

Android and iOS
----------------
- Added video sharing

iOS
---
- Enhanced network calls (Android)

-------------------------------------------------------------
1.0.6
-------------------------------------------------------------

Android
--------
- Reduced dependencies
- Removed unused classes and functions

-------------------------------------------------------------
1.0.5
-------------------------------------------------------------

Android and iOS
----------------
- Bug fixes

-------------------------------------------------------------
1.0.4
-------------------------------------------------------------

Android and iOS
----------------
- Added image sharing
- Revised login function
- Bug fixes

-------------------------------------------------------------
1.0.3
-------------------------------------------------------------

Android and iOS
----------------
- Added error codes for block/unblock feature
- Function name to fetch blocked users changed from getBlockedUsers to getBlockedUserList

-------------------------------------------------------------
1.0.2
-------------------------------------------------------------

Android and iOS
----------------
- Added user block/unblock feature
- Added ability to fetch blocked users
- Bug fixes

-------------------------------------------------------------
1.0.1
-------------------------------------------------------------

Android and iOS
----------------
Bug: \' slashes bug
Bug: Slowing of SDK due to large number of friends

-------------------------------------------------------------
1.0.0
-------------------------------------------------------------

Android and iOS
----------------
Initial release of SDK
