//
//  CommentTableViewCell.h
//  friendsquare
//
//  Created by magnon on 02/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell{
}

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *datetimeLbl;
@property (weak, nonatomic) IBOutlet UILabel *commenttextLbl;

@property (weak, nonatomic) IBOutlet UIImageView *picView;

@end
