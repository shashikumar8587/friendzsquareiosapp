//
//  FRequestCell.h
//  friendsquare
//
//  Created by Prabhat Pankaj on 1/7/16.
//  Copyright © 2016 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRequestCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnRemove;
@end
