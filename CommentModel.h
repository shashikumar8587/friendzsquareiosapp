//
//  CommentModel.h
//  friendsquare
//
//  Created by magnon on 09/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <JSONModel/JSONModel.h>
@protocol CommentModel
@end
@interface CommentModel : JSONModel
{
    
}
//@property (strong, nonatomic) PostImagesFeed* image;
@property (strong, nonatomic) NSString* comment;
@property (strong, nonatomic) NSString* comment_id;
@property (strong, nonatomic) NSString* created;
@property (strong, nonatomic) NSString* u_first_name;
@property (strong, nonatomic) NSString* u_last_name;
@property (strong, nonatomic) NSString* u_profile_photo;
@property (strong, nonatomic) NSString* u_id;
@property (strong, nonatomic) NSString* wall_id;

@end



