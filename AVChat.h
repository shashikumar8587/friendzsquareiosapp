/*
 
 CometChat
 Copyright (c) 2015 Inscripts
 
 CometChat ('the Software') is a copyrighted work of authorship. Inscripts
 retains ownership of the Software and any copies of it, regardless of the
 form in which the copies may exist. This license is not a sale of the
 original Software or any copies.
 
 By installing and using CometChat on your server, you agree to the following
 terms and conditions. Such agreement is either on your own behalf or on behalf
 of any corporate entity which employs you or which you represent
 ('Corporate Licensee'). In this Agreement, 'you' includes both the reader
 and any Corporate Licensee and 'Inscripts' means Inscripts (I) Private Limited:
 
 CometChat license grants you the right to run one instance (a single installation)
 of the Software on one web server and one web site for each license purchased.
 Each license may power one instance of the Software on one domain. For each
 installed instance of the Software, a separate license is required.
 The Software is licensed only to you. You may not rent, lease, sublicense, sell,
 assign, pledge, transfer or otherwise dispose of the Software in any form, on
 a temporary or permanent basis, without the prior written consent of Inscripts.
 
 The license is effective until terminated. You may terminate it
 at any time by uninstalling the Software and destroying any copies in any form.
 
 The Software source code may be altered (at your risk)
 
 All Software copyright notices within the scripts must remain unchanged (and visible).
 
 The Software may not be used for anything that would represent or is associated
 with an Intellectual Property violation, including, but not limited to,
 engaging in any activity that infringes or misappropriates the intellectual property
 rights of others, including copyrights, trademarks, service marks, trade secrets,
 software piracy, and patents held by individuals, corporations, or other entities.
 
 If any of the terms of this Agreement are violated, Inscripts reserves the right
 to revoke the Software license at any time.
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 
 */

#import <Foundation/Foundation.h>

@interface AVChat : NSObject

/* Start AVChat with given callID and within given container */
- (void)startAVChatWithCallID:(NSString *)callID
                containerView:(UIView *)container
                connectedUser:(void(^)(NSDictionary *response))response
           changeInAudioRoute:(void(^)(NSDictionary *audioRouteInformation))audioRouteInformation
                      failure:(void(^)(NSError *error))failure;

/* Send AVChat request to user */
- (void)sendAVChatRequestToUser:(NSString *)userID
                        success:(void(^)(NSDictionary *response))response
                        failure:(void(^)(NSError *error))failure;

/* Accept AVChat request from user with give callID */
- (void)acceptAVChatRequestOfUser:(NSString *)userID
                           callID:(NSString *)callID
                          success:(void(^)(NSDictionary *response))response
                          failure:(void(^)(NSError *error))failure;

/* End AVChat call with user with give callID */
- (void)endAVChatWithUser:(NSString *)userID
                   callID:(NSString *)callID
                  success:(void(^)(NSDictionary *response))response
                  failure:(void(^)(NSError *error))failure;

/* Cancel AVChat call request with user */
- (void)cancelAVChatRequestWithUser:(NSString *)userID
                            success:(void(^)(NSDictionary *response))response
                            failure:(void(^)(NSError *error))failure;

/* Send Busy call request to user */
- (void)sendBusyCallToUser:(NSString *)userID
                   success:(void(^)(NSDictionary *response))response
                   failure:(void(^)(NSError *error))failure;

/* Reject AVChat call request */
- (void)rejectAVChatRequestOfUser:(NSString *)userID
                           callID:(NSString *)callID
                          success:(void(^)(NSDictionary *response))response
                          failure:(void(^)(NSError *error))failure;

/* Send No answer call request */
- (void)sendNoAnswerCallOfUser:(NSString *)userID
                        callID:(NSString *)callID
                       success:(void(^)(NSDictionary *response))response
                       failure:(void(^)(NSError *error))failure;

/* Toggle Audio to ON/OFF state */
- (void)toggleAudio:(BOOL)audioControlFlag;

/* Toggle Video to ON/OFF state */
- (void)toggleVideo:(BOOL)videoControlFlag;

/* Switch audio route to Ear-piece/Speaker */
- (void)switchAudioRoute;

/* Switch between Front/Rear camera */
- (void)switchCamera;

@end
