//
//  imgCollectionViewCell.h
//  friendsquare
//
//  Created by magnon on 30/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface imgCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UIButton *crossBtn;

@end
