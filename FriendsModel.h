//
//  FriendsModel.h
//  friendsquare
//
//  Created by magnon on 17/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//


#import <JSONModel/JSONModel.h>
#import "FriendsModelKeys.h"

@interface FriendsModel : JSONModel
@property (strong, nonatomic) NSMutableArray<FriendsModelKeys>* items;
@property (strong,nonatomic)NSString *current_user_count;
@end
