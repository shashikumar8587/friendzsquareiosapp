//
//  CommentList.h
//  friendsquare
//
//  Created by magnon on 09/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "CommentModel.h"
@interface CommentList : JSONModel{
    
}
@property (strong, nonatomic) NSMutableArray<CommentModel>* comments;


@end
