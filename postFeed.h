//
//  postFeed.h
//  friendsquare
//
//  Created by magnon on 04/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "PostModel.h"
//#import "postImagesfeed.h"
@interface postFeed : JSONModel

@property (strong, nonatomic) NSMutableArray<PostModel>* feed;
//@property (strong, nonatomic) NSMutableArray<postImagesfeed>* post_image_list;
@end
