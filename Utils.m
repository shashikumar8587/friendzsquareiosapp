//
//  Utils.m
//  friendsquare
//
//  Created by magnon on 26/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "Utils.h"
@implementation Utils


+ (UIImage *)compressImage:(UIImage *)image{
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 250.0;
    float maxWidth = 450.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.7;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth) {
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}



+(void )showAlertwithMessage:(NSString *)msg{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:App_title message:msg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alert show];
}
+(NSString *)fullName :(NSString *)firstName :(NSString *)lastName
{
    NSString *fullName;
    
    fullName=firstName;
   fullName= [fullName stringByAppendingString:@" "];
     fullName= [fullName stringByAppendingString:lastName];
//    NSLog(@"first name is %@",firstName);
//     NSLog(@"Last name is %@",lastName);
    return fullName;
}
+(NSArray *)imagesArray:(NSString *)str{
    NSString *imagesString=str;
  //  NSMutableArray *arr=[[NSMutableArray alloc]init];
    NSArray *imgArray=[imagesString componentsSeparatedByString: @";"];
   // [arr addObjectsFromArray:imgArray];
    return imgArray;
}
+(NSString *)completeaddress :(NSString *)state :(NSString *)country{
    NSString *addressStr;
    if([state isEqualToString:@""] && [country isEqualToString:@""]){
        addressStr=@"";
    }
    else if([state isEqualToString:@""] && ![country isEqualToString:@""]){
        addressStr=country;
    }
    else if(![state isEqualToString:@""] && [country isEqualToString:@""]){
        addressStr=state;
    }
    else if(![state isEqualToString:@""] && ![country isEqualToString:@""]){
        
       
        state=[state stringByAppendingString:@","];
        state=[state stringByAppendingString:country];
        
        addressStr=state;
        

            }

    return addressStr;
    
    
}
@end
