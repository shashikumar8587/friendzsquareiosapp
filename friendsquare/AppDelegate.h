//
//  AppDelegate.h
//  friendsquare
//
//  Created by magnon on 12/10/15.
//  Copyright (c) 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Fabric/Fabric.h>
#import <TwitterKit/TwitterKit.h>
#import <Google/SignIn.h>


@class GIDProfileData;
@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate,UITabBarControllerDelegate>{
    BOOL isFacebook;
}
@property(nonatomic, assign) BOOL isCancelSearch;
@property(nonatomic,assign)BOOL isFacebook;
@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic)GIDProfileData *pd;
@property(strong,nonatomic) NSString *url_image;
-(void)logout;
+ (AppDelegate*) GetAppDelegate;
@property (nonatomic,strong)  UITabBarController *TabBarController;
@property (nonatomic,strong) UIImageView *imageReport;
@property (nonatomic,strong) UIImageView *imageStatus;
@property (nonatomic,strong) UIImageView *imageUpdate;
@property (strong,nonatomic) NSMutableArray *array1;

-(void)tabShow;
@end

