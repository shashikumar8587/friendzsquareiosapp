//
//  AppDelegate.m
//  friendsquare
//
//  Created by magnon on 12/10/15.
//  Copyright (c) 2015 magnon. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "userDetail.h"
#import "DataManager.h"
#import "ViewController.h"
#import "Constants.h"
#import <Parse/Parse.h>

#import "OneOnOneViewController.h"
#import "userDetail.h"




#import "HomeViewController.h"
#import "Connections.h"
#import "Constants.h"


#import "ProfileViewController.h"

#import "Utils.h"
#import "MyProfileViewController.h"
#import "OneOnOneViewController.h"
#import "ChatroomViewController.h"
#import "FriendRequest.h"
#import "Settings.h"
#import "PushNotification.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize isFacebook,isCancelSearch,TabBarController;
static NSString * const kClientID = @"362701957086-bnpakmbn1s76l7phje94ri6h6ec2n3et.apps.googleusercontent.com";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    /* Push notification Registration */
    [PushNotification registerParse];
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [application registerForRemoteNotifications];
        
    } else {
        
        [application registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    
    
    [Fabric with:@[[Twitter class]]];

    // Override point for customization after application launch.
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    NSError* configureError;
    [[GGLContext sharedInstance] configureWithError: &configureError];
    NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
    
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].shouldFetchBasicProfile=YES;
    [GIDSignIn sharedInstance].clientID=kClientID;
    return YES;
}
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken{
    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken with devicetoken : %@",deviceToken);
    if (deviceToken != nil) {
        NSLog(@"registering for push notification");
        [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"devicetoken"];
        [PushNotification registerDeviceToken:[[NSUserDefaults standardUserDefaults] objectForKey:@"devicetoken"]];
    }
    
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    if (error.code == 3010) {
        NSLog(@"Push notifications are not supported in the iOS Simulator.");
    } else {
        // show some alert or otherwise handle the failure to register.
        NSLog(@"application:didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"PUSH NOTIFICATION DATA1 = %@",userInfo);
    NSLog(@"PUSH NOTIFICATION DATA2 = %@",[[NSUserDefaults standardUserDefaults] objectForKey:@"devicetoken"]);
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"devicetoken"] != nil) {
        
        NSLog(@"PUSH NOTIFICATION DATA1 = %@",userInfo);
        [PFPush handlePush:userInfo];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
        
        PFInstallation *currentInstallation = [PFInstallation currentInstallation];
        
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
        
    }
    
    //[[UAPush shared] resetBadge];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    NSLog(@"PUSH NOTIFICATION DATA2 = %@",userInfo);
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    
    currentInstallation.badge = 0;
    [currentInstallation saveEventually];
    
    
    if (application.applicationState != UIApplicationStateActive) {
        
        if (userInfo != nil) {
            
            
            
        }
        else{
            NSLog(@"NULL DATA IN APP LAUNCH");
        }
        
        
    }
    completionHandler(UIBackgroundFetchResultNoData);
    
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    if(isFacebook){
        isFacebook=NO;
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:sourceApplication
                                                           annotation:annotation
                ];
       

    }
    
    else{
       return  [[GIDSignIn sharedInstance] handleURL:url
                            sourceApplication:sourceApplication
                                   annotation:annotation];
    
    }
    return 0;
}
//- (BOOL)application:(UIApplication *)application
//            openURL:(NSURL *)url
//  sourceApplication:(NSString *)sourceApplication
//         annotation:(id)annotation {
//    //    if (url) {
//    //        HomeViewController *homeCont = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
//    //        [navCont pushViewController:homeCont animated:NO];
//    //    }
//    return [[GIDSignIn sharedInstance] handleURL:url
//                               sourceApplication:sourceApplication
//                                      annotation:annotation];
//    
//}

// [START signin_handler]
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;
    if([userId length]==0){
    }
    else{
        NSLog(@"user id %@",userId);
        // For client-side use only!
        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *name = user.profile.name;
        NSString *email = user.profile.email;
        //NSString *fName=user.profile.
        
        _url_image=  [NSString stringWithFormat:@"%@",[user.profile imageURLWithDimension:50]];
        NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
        [userDefault setObject:_url_image forKey:@"Userimage"];
        [userDefault setObject:name forKey:@"FULLNAME"];
        [userDefault setObject:email forKey:@"EMAIL"];
        [userDefault setObject:userId forKey:@"social_id"];
       
        NSDictionary *statusText = @{@"statusText":
                                         [NSString stringWithFormat:@"Signed in user: %@",
                                          name]};
        // [userDefault setValue:name forKey:@"USERNAME"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"connectWithGoogle"
         object:nil
         userInfo:statusText];
    }
    // [END_EXCLUDE]
}
// [END signin_handler]

// This callback is triggered after the disconnect call that revokes data
// access to the user's resources has completed.
// [START disconnect_handler]
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // [START_EXCLUDE]
    //NSDictionary *statusText = @{@"statusText": @"Disconnected user" };
    //    [[NSNotificationCenter defaultCenter]
    //     postNotificationName:@"connectWithGoogle"
    //     object:nil
    //     userInfo:statusText];
    // [END_EXCLUDE]
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
-(void)logout{
    userDetail *user=[_gAppData userObject];
    [user destroyPreference];
    
    
   
}
+ (AppDelegate*) GetAppDelegate{
    return (AppDelegate*)([UIApplication sharedApplication].delegate);
}
-(void)tabShow
{
    for(UIView *view in self.window.subviews){
        [view removeFromSuperview];
    }
    
    UINavigationController *localNavigationController;
    NSMutableArray *localControllersArray=[[NSMutableArray alloc]initWithCapacity:7];
    self.TabBarController = [[UITabBarController alloc] init];
    
    self.TabBarController.delegate=self;
    
    {
      

        HomeViewController *home;
        Connections *connections;
        ChatroomViewController *chatRoom;
        MyProfileViewController *profile;
        OneOnOneViewController *chat;
        FriendRequest *friendsRequestView;
        Settings *settingVC;
        //[TabBarController.tabBar setBackgroundColor:[UIColor greenColor]];
      // CGRect rect= [TabBarController.tabBar frame];
      //  rect.size.height=166.0;
      //  [TabBarController.tabBar setFrame:rect];
        [TabBarController.tabBar setTintColor:[UIColor colorWithRed:62/255.0 green:169/255.0 blue:67/255.0 alpha:1]];
        
            home=[MainStoryBoard instantiateViewControllerWithIdentifier: @"Home"];
            UITabBarItem *item0 = [[UITabBarItem alloc ]initWithTitle:@"New Feeds" image:[UIImage imageNamed:@"NewFeeds"] tag:0];
            home.tabBarItem = item0;
            localNavigationController=[[UINavigationController alloc]initWithRootViewController:home];
            [localControllersArray addObject:localNavigationController];
            
        
            
            connections=[MainStoryBoard instantiateViewControllerWithIdentifier: @"Connections"];
            UITabBarItem *item1 = [[UITabBarItem alloc ]initWithTitle:@"Friends" image:[UIImage imageNamed:@"Friends"] tag:1];
               connections.tabBarItem = item1;

            localNavigationController=[[UINavigationController alloc] initWithRootViewController:connections];
            
                       [localControllersArray addObject:localNavigationController];
            
            
            
             chatRoom=[MainStoryBoard instantiateViewControllerWithIdentifier: @"chatroomviewcontroller"];
            localNavigationController=[[UINavigationController alloc]initWithRootViewController:chatRoom];
        UITabBarItem *item2 = [[UITabBarItem alloc ]initWithTitle:@"Chat Rooms" image:[UIImage imageNamed:@"ChatRooms"] tag:2];
        chatRoom.tabBarItem = item2;
        [localControllersArray addObject:localNavigationController];
            
            
        profile=[MainStoryBoard instantiateViewControllerWithIdentifier: @"MyProfileViewController"];
        localNavigationController=[[UINavigationController alloc]initWithRootViewController:profile];
        UITabBarItem *item3 = [[UITabBarItem alloc ]initWithTitle:@"Profile" image:[UIImage imageNamed:@"Profile"] tag:3];
        profile.tabBarItem = item3;
        [localControllersArray addObject:localNavigationController];
        
        
        chat=[MainStoryBoard instantiateViewControllerWithIdentifier: @"oneononeviewcontroller"];
        localNavigationController=[[UINavigationController alloc]initWithRootViewController:chat];
        UITabBarItem *item4 = [[UITabBarItem alloc ]initWithTitle:@"Chat" image:[UIImage imageNamed:@"Chat"] tag:4];
        chat.tabBarItem = item4;
        [localControllersArray addObject:localNavigationController];
        
        friendsRequestView=[MainStoryBoard instantiateViewControllerWithIdentifier: @"FriendRequest"];
        localNavigationController=[[UINavigationController alloc]initWithRootViewController:friendsRequestView];
        UITabBarItem *item5 = [[UITabBarItem alloc ]initWithTitle:@"FriendRequest" image:[UIImage imageNamed:@"FriendsRequest"] tag:5];
        friendsRequestView.tabBarItem = item5;
        [localControllersArray addObject:localNavigationController];
       
        settingVC=[MainStoryBoard instantiateViewControllerWithIdentifier: @"Settings"];
        localNavigationController=[[UINavigationController alloc]initWithRootViewController:settingVC];
        UITabBarItem *item6 = [[UITabBarItem alloc ]initWithTitle:@"Settings" image:[UIImage imageNamed:@"Settings"] tag:6];
        settingVC.tabBarItem = item6;
        [localControllersArray addObject:localNavigationController];
        
        
    }
    [self.TabBarController setViewControllers:localControllersArray];
    [self.window removeFromSuperview];
    [self.window addSubview:self.TabBarController.view];
    
    
}


@end
