//
//  ViewController.h
//  friendsquare
//
//  Created by magnon on 12/10/15.
//  Copyright (c) 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface ViewController : UIViewController

- (IBAction)loginMethod:(id)sender;
- (IBAction)signUp:(id)sender;

@end

