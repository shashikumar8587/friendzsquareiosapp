//
//  ViewController.m
//  friendsquare
//
//  Created by magnon on 12/10/15.
//  Copyright (c) 2015 magnon. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"
#import "Constants.h"
#import "RegisterViewController.h"
#import "HomeViewController.h"
#import "AppDelegate.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    self.navigationController.navigationBarHidden=YES;
    if (GET_USERID) {
        
//        NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
//        [navigationArray removeAllObjects];
        [[AppDelegate GetAppDelegate] tabShow];
        HomeViewController *objHome =[self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        [self.navigationController pushViewController:objHome animated:NO];
        objHome=nil;
        
    }
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated{
    [[AppDelegate GetAppDelegate] .TabBarController.tabBar setHidden:YES];
    //[appDelegate.TabBarController.tabBar setHidden:YES];
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
    [navigationArray removeAllObjects];
    self.navigationController.navigationBarHidden=YES;

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginMethod:(id)sender {
    LoginViewController *login=(LoginViewController *)[MainStoryBoard instantiateViewControllerWithIdentifier:@"Login"];
    // login.user=user;
    [self.navigationController pushViewController:login animated:YES];
    login=nil;
}

- (IBAction)signUp:(id)sender {
    RegisterViewController *registerVc=(RegisterViewController *)[MainStoryBoard instantiateViewControllerWithIdentifier:@"Register"];
    // login.user=user;
    [self.navigationController pushViewController:registerVc animated:YES];
    registerVc=nil;
}
@end
