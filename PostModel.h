//
//  PostModel.h
//  friendsquare
//
//  Created by magnon on 04/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "postImagesfeed.h"

@protocol PostModel
@end
@interface PostModel : JSONModel
{
    
}
@property (strong, nonatomic) NSMutableArray<postImagesfeed> *post_image_list;
@property (strong, nonatomic) NSString* image_url;
//@property (strong, nonatomic) NSString* image;
@property (strong, nonatomic) NSString* wall_id;
@property (strong, nonatomic) NSString* content;
@property (strong, nonatomic) NSString* created;
@property (strong, nonatomic) NSString* u_id;
@property (strong, nonatomic) NSString* u_first_name;
@property (strong, nonatomic) NSString* u_last_name;
@property (strong, nonatomic) NSString* u_profile_photo;
@property (strong, nonatomic) NSString* total_like;
@property (strong, nonatomic) NSString* total_comments;
@end
