//
//  userDetail.m
//  friendsquare
//
//  Created by magnon on 23/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "userDetail.h"
#import "Constants.h"
@implementation userDetail


// destroy the preferences of the user
- (void) destroyPreference
{
    _total_connection=nil;
    _u_id =nil;
    _u_display_name =nil;
    _u_first_name = nil;
    _u_last_name =nil;
    _u_email =nil;
    _u_gender =nil;
    _u_personal_visibility =nil;
    _u_marital_status = nil;
    _u_profile_photo = nil;
    _u_country =nil;
    _u_state  = nil;
    _u_city = nil;
    _u_zip_code = nil;
    _u_phone_number = nil;
    _u_qualification_visibility = nil;
    _u_education_level = nil;
    _u_college_specialization = nil;
    _u_graduate_year  = nil;
    _u_study_status = nil;
    _u_city_establishment = nil;
    _u_education_establishment  = nil;
    _u_professional_visibility = nil;
    _u_current_profession = nil;
    _u_sub_profession = nil;
    _u_current_position = nil;
    _u_doc = nil;
    
    
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults removeObjectForKey:FSUserid];
    
    //[defaults setObject:nil forKey:GSUserId];
    [defaults removeObjectForKey:FSTotalConnection];
    [defaults removeObjectForKey:FSDisplayname];
    [defaults removeObjectForKey:FSfirstname];
    [defaults removeObjectForKey:FSlastname];
    [defaults removeObjectForKey:FSemail];
    [defaults removeObjectForKey:FSgender];
    [defaults removeObjectForKey:FSpersonalVisibility];
    [defaults removeObjectForKey:FSmaritalstatus];
    
    [defaults removeObjectForKey:FSprofilePhoto];
    [defaults removeObjectForKey:FScountry];
    [defaults removeObjectForKey:FSState];
    [defaults removeObjectForKey:FSCity];
    [defaults removeObjectForKey:FSZipCode];
    [defaults removeObjectForKey:FSPhoneNumber];
    [defaults removeObjectForKey:FSQualificationVisibility];
    [defaults removeObjectForKey:FSEducationLevel];
    [defaults removeObjectForKey:FSCollegeSpecialization];
    [defaults removeObjectForKey:FSGraduateYear];
    [defaults removeObjectForKey:FSStudyStatus];
    [defaults removeObjectForKey:FSCityEstablishment];
    [defaults removeObjectForKey:FSEducationEstablishment];//Current_User_ID
    [defaults removeObjectForKey:FSProfessionalVisibility];
    
    [defaults removeObjectForKey:FSCurrentProfession];
    [defaults removeObjectForKey:FSSubProfession];
    [defaults removeObjectForKey:FSCurrentPosition];//Current_User_ID
    [defaults removeObjectForKey:FSUDoc];
    
    
    [defaults removeObjectForKey:@"Userimage"];
    [defaults removeObjectForKey:@"FULLNAME"];
    [defaults removeObjectForKey:@"EMAIL"];//Current_User_ID
    [defaults removeObjectForKey:@"social_id"];

    
    [defaults synchronize];
    
    //For UserID;
    //    NSUserDefaults *defLog=[NSUserDefaults standardUserDefaults];
    //    [defLog setObject:nil forKey:@"user_id"];
    //    [defLog synchronize];
}


- (id) init
{
    if (self = [super init])
    {
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
       
       
                _total_connection=[defaults objectForKey:FSTotalConnection];
        _u_id = [defaults objectForKey:FSUserid];
        _u_display_name = [defaults objectForKey:FSDisplayname];
        _u_first_name = [defaults objectForKey:FSfirstname];
        _u_last_name  = [defaults objectForKey:FSlastname];
        _u_email = [defaults objectForKey:FSemail];
        _u_gender  = [defaults objectForKey:FSgender];
        _u_personal_visibility = [defaults objectForKey:FSpersonalVisibility];
        _u_marital_status = [defaults objectForKey:FSmaritalstatus];
        _u_profile_photo  = [defaults objectForKey:FSprofilePhoto];
        _u_country = [defaults objectForKey:FScountry];
        _u_state = [defaults objectForKey:FSState];
        _u_city  = [defaults objectForKey:FSCity];
        _u_zip_code = [defaults objectForKey:FSZipCode];
        _u_phone_number = [defaults objectForKey:FSPhoneNumber];
        _u_qualification_visibility  = [defaults objectForKey:FSQualificationVisibility];
        _u_education_level = [defaults objectForKey:FSEducationLevel];
        _u_college_specialization = [defaults objectForKey:FSCollegeSpecialization];
        _u_graduate_year  = [defaults objectForKey:FSGraduateYear];
        _u_study_status = [defaults objectForKey:FSStudyStatus];
        _u_city_establishment = [defaults objectForKey:FSCityEstablishment];
        _u_education_establishment  = [defaults objectForKey:FSEducationEstablishment];
        _u_professional_visibility = [defaults objectForKey:FSProfessionalVisibility];
        _u_current_profession = [defaults objectForKey:FSCurrentProfession];
        _u_sub_profession  = [defaults objectForKey:FSSubProfession];
        _u_current_position = [defaults objectForKey:FSCurrentPosition];
        _u_doc = [defaults objectForKey:FSUDoc];
        
        [defaults synchronize];
        
        
      
        
        
        
        
    }
    return self;
}







@end
