//
//  ProfileViewController.h
//  friendsquare
//
//  Created by magnon on 26/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

#import "FriendsModel.h"
#import "FriendsModelKeys.h"
@interface ProfileViewController : UIViewController{
   
    FriendsModel *_items;
}
- (IBAction)RequestUpdate:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
- (IBAction)logoutMethod:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *picView;
@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UILabel *designationLbl;
@property (weak, nonatomic) IBOutlet UILabel *addressLbl;
@property (weak, nonatomic) IBOutlet UILabel *friendsLbl;
@property(strong,nonatomic)FriendsModelKeys *selectedFriend;
@end
