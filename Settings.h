//
//  Settings.h
//  friendsquare
//
//  Created by magnon on 10/01/16.
//  Copyright © 2016 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CometChatSDK/CometChat.h>
#import "NativeKeys.h"
#import "PushNotification.h"
#import <CometChatSDK/CometChatChatroom.h>

@interface Settings : UIViewController{
 CometChat *cometChat;
}
@end
