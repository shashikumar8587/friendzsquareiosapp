//
//  HomeViewController.m
//  friendsquare
//
//  Created by magnon on 03/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "HomeViewController.h"

#import "UINavigationBar+navigationBar.h"
#import "DashboardTableViewCell.h"
#import "imgCollectionViewCell.h"
#import "GMImagePickerController.h"
#import "CommentsViewController.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "Status.h"
#import "Utils.h"
#import "Base64.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "NSString+Additions.h"
#import "NSDate+Additions.h"
#import "PeopleYouKnowCell.h"

#import "NativeKeys.h"
#import <Parse/Parse.h>
#import "OneOnOneViewController.h"
#import "RDImageViewerController.h"
#import <CometChatSDK/CometChat.h>
#import "NativeKeys.h"
#import "OneOnOneViewController.h"
#import "ChatroomViewController.h"
#import "PushNotification.h"
#import <CometChatSDK/CometChatChatroom.h>
#import "ChatroomChatViewController.h"




@import UIKit;
@import Photos;
@interface HomeViewController ()<UIImagePickerControllerDelegate,GMImagePickerControllerDelegate,UINavigationControllerDelegate>{
    NSMutableArray *imgArray;
    BOOL isHelpViewVisible ;
    CGRect originalFrame;
    NSArray *imageArray;
    CometChatChatroom *cometChatRoom;
    CometChat *cometChat;
    RDImageViewerController *viewController;
    BOOL isImageScroller;
   
}
@property(strong,nonatomic)NSMutableArray *imgArray;
@end

@implementation HomeViewController
@synthesize postImages,imgArray,postListArray;


- (void) myClassDelegateMethod:(NSInteger)index :(NSIndexPath *)ind{
  //  NSLog(@"selected Indexpath is %@",ind);
    
    PostModel *post = _feed.feed[ind.row];
    
  //  NSLog(@"Post array on select is %@",post.post_image_list);

    postImagesfeed *feedImages;

    NSMutableArray *showArray=[[NSMutableArray alloc]init];
    for (feedImages in post.post_image_list) {
        
    
        [showArray addObject:feedImages.postimg_image];
    
    }
 
    isImageScroller=YES;
    
    
    for (int i = 0; i<showArray.count; i++) {
        viewController = [[RDImageViewerController alloc] initWithRemoteImageHandler:^NSURLRequest *(NSInteger pageIndex) {
            NSString *imageString= [NSString stringWithFormat:@"%@",[KBaseUrlPostListImages stringByAppendingString:[showArray objectAtIndex:pageIndex]]];
            return
            [NSURLRequest requestWithURL:[NSURL URLWithString:imageString]];
        } numberOfImages:showArray.count direction:RDPagingViewForwardDirectionRight];
    }
    viewController.autoBarsHiddenDuration = 1;
    viewController.showSlider = FALSE;
    viewController.showPageNumberHud = TRUE;
    viewController.landscapeMode = RDImageScrollViewResizeModeAspectFit;
    viewController.loadAsync = YES;
    viewController.currentPageIndex = index;
    [self.navigationController pushViewController:viewController animated:YES];
    viewController=nil;





}
- (void)viewDidLoad {
     cometChat = [[CometChat alloc] initWithAPIKey:@""];
     [CometChat setDevelopmentMode:NO];
    cometChatRoom = [[CometChatChatroom alloc] init];

  //   NSLog(@"SDK Version is %@",[CometChat getSDKVersion]);
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:BUDDY_LIST];
    
    /* Remove previous log from user defaults */
    [[NSUserDefaults standardUserDefaults]removeObjectForKey:LOG_LIST];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGGED_IN_USER];
    
    
    /* Subscribe to One On One Chat ONLY AFTER successful login.  Set mode argument to YES if you want to strip html elements */
    [cometChat subscribeWithMode:YES onMyInfoReceived:^(NSDictionary *response) {
        
        [PushNotification subscribeToParseChannel:[response objectForKey:@"push_channel"]];
        
       // NSLog(@"SDK log : OneOnOne MYInfo %@",response);
        
        if ([response objectForKey:ID]) {
            
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[response objectForKey:ID]] forKey:LOGGED_IN_USER];
        }
        
    } onGetOnlineUsers:^(NSDictionary *response) {
        
        /* Online users list will be received here */
       // NSLog(@"SDK log : OneOnOne onGetOnlineUsers %@",response);
        
        [NativeKeys getLogOType:LOG_TYPE_ONE_ON_ON ForMessage:@"onGetOnlineUsers"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.inscripts.logsview.refreshLogs" object:nil];
        
     //   NSLog(@"reponse.allKeys : %@  ",response.allKeys);
        
        /* Update buddylist table */
        NSMutableArray *buddyList = [[NSMutableArray alloc] init];
        NSArray *users = response.allKeys;
        
        for (int i = 0 ; i < [users count]; i++) {
            
            if ([response objectForKey:[users objectAtIndex:i]]) {
                [buddyList addObject:[response objectForKey:[users objectAtIndex:i]]];
            }
        }
     //
     //   NSLog(@"BuddyList Check : %@",buddyList);
        
        /* Store buddyList in userdefaults */
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:buddyList] forKey:BUDDY_LIST];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        /* Send notification to OneOnOneView to refresh buddyList */
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.inscripts.oneononeview.refreshBuddyList" object:nil];
        
    } onMessageReceived:^(NSDictionary *response) {
        
        /* One On One messages will be recieved in this callback */
        NSLog(@"SDK log : OneOnOne onMessageReceived %@",response);
        
        [NativeKeys getLogOType:LOG_TYPE_ONE_ON_ON ForMessage:@"onMessageReceived"];
        
        NSLog(@"Current Buddy : %@ : From : %@",[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_BUDDY_ID],[response objectForKey:FROM]);
        if ([[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_BUDDY_ID]) {
            
            /* If message is received from current buddy (Buddy you are chatting with), then send notification to OneOnOneChatView controller */
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_BUDDY_ID] isEqualToString:[NSString stringWithFormat:@"%@",[response objectForKey:FROM]]]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"com.inscripts.oneononechat.messagereceived" object:nil userInfo:response];
            }
        }
        
    } onAnnouncementReceived:^(NSDictionary *response) {
        
        NSLog(@"SDK log : OneOnOne announcement %@",response);
        
        /* Announcements messages will be recieved in this callback */
        [NativeKeys getLogOType:LOG_TYPE_ONE_ON_ON ForMessage:@"onAnnouncementReceived"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.inscripts.logsview.refreshLogs" object:nil];
        
    } onAVChatMessageReceived:^(NSDictionary *response) {
        NSLog(@"SDK log : AVChat message received = %@",response);
        
        [NativeKeys getLogOType:LOG_TYPE_AVCHAT ForMessage:@"onAVChatMessageReceived"];
        
        if ([[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_BUDDY_ID]] isEqualToString:[NSString stringWithFormat:@"%@",[response objectForKey:FROM]]] || ([[response objectForKey:MESSAGE_TYPE_KEY] integerValue] == 33)) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"com.demosdkproject.handleavchatcalls" object:nil userInfo:response];
        }
        
    } onActionMessageReceived:^(NSDictionary *response) {
        NSLog(@"SDK Log : onActionMessageReceived = %@",response);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.demosdkproject.handleactionmessagecalls" object:nil userInfo:response];
    } failure:^(NSError *error) {
        /* Subscribe failure will be handled here */
        NSLog(@"SDK log : OneOnOne subscribe error %@",error);
        [NativeKeys getLogOType:LOG_TYPE_ONE_ON_ON ForMessage:@"Subscribe failure"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.inscripts.logsview.refreshLogs" object:nil];
        
    }];
    
    _startNo=0;
    _limit=10;
    
    postListArray=[[NSMutableArray alloc]init];
    postImages=[[NSMutableArray alloc]init];
    imageArray=[[NSArray alloc]init];
    [_uploadCollectionView setHidden:YES];
    [_suggestionView setHidden:YES];
    _postTbl.separatorColor = [UIColor clearColor];
    imgArray =[[NSMutableArray alloc]init];
    self.view.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1];
    
    _peopleyouknowCollectionView.delegate=self;
    [self cometChatroomSetup];
     [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)cometChatroomSetup{
    /* Subscribe to Chatroom set mode argument to YES if you want to strip html elements */
    
    [cometChatRoom subscribeToChatroomWithMode:YES
     
                     onChatroomMessageReceived:^(NSDictionary *response) {
                         /* Chatroom messages will be recieved in this callback */
                         NSLog(@"SDK log : chatroom onChatroomMessageReceived %@",response);
                         
                         [NativeKeys getLogOType:LOG_TYPE_CHATROOM ForMessage:@"onChatroomMessageReceived"];
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"com.sdkdemo.logsview.refreshLogs" object:nil];
                         
                         /* Notify if user has joined any chatrom */
                         if ([[NSUserDefaults standardUserDefaults] objectForKey:CURRENT_CHATROOM_ID]) {
                             
                             NSMutableDictionary *tempdic = [NSMutableDictionary dictionaryWithDictionary:response];
                             
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"com.sdkdemo.chatroomchat.messagereceived" object:nil userInfo:tempdic];
                             
                             
                         }
                         
                     } onActionMessageReceived:^(NSDictionary *response) {
                         /* Callback block for actions in chatroom */
                         
                         NSLog(@"SDK log : on Action Message received %@",response);
                         
                         if ([[response objectForKey:@"action_type"] isEqualToString:@"10"]) {
                             NSLog(@"SDK log : chatroom onKicked %@",response);
                             
                             [NativeKeys getLogOType:LOG_TYPE_CHATROOM ForMessage:@"onKicked"];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"com.sdkdemo.logsview.refreshLogs" object:nil];
                             
                         } else if ([[response objectForKey:@"action_type"] isEqualToString:@"11"]) {
                             NSLog(@"SDK log : chatroom onBanned %@",response);
                             
                             [NativeKeys getLogOType:LOG_TYPE_CHATROOM ForMessage:@"onBanned"];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"com.sdkdemo.logsview.refreshLogs" object:nil];
                             
                         }
                     }
                       onChatroomsListReceived:^(NSDictionary *response) {
                           
                           /* Chatrooms list will be received here */
                           NSLog(@"SDK log : chatroom chatroomsListReceived %@",response);
                           
                           [NativeKeys getLogOType:LOG_TYPE_CHATROOM ForMessage:@"chatroomsListReceived"];
                           [[NSNotificationCenter defaultCenter] postNotificationName:@"com.sdkdemo.logsview.refreshLogs" object:nil];
                           
                           /* Update Chatrooms table */
                           
                           NSMutableArray *chatRoomList = [[NSMutableArray alloc] init];
                           NSArray *chatrooms = response.allKeys;
                           
                           for (int i = 0 ; i < [chatrooms count]; i++) {
                               
                               if ([response objectForKey:[chatrooms objectAtIndex:i]]) {
                                   [chatRoomList addObject:[response objectForKey:[chatrooms objectAtIndex:i]]];
                               }
                           }

                           [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:chatRoomList] forKey:CHATROOMLIST];
                           
                           [[NSUserDefaults standardUserDefaults] synchronize];
                           
                           /* Send notification to OneOnOneView to refresh buddyList */
                           [[NSNotificationCenter defaultCenter] postNotificationName:@"com.inscripts.oneononeview.chatroomList" object:nil];
                           
                       } onChatroomMembersListReceived:^(NSDictionary *response) {
                           
                           /* Chatrooms list will be received here */
                           NSLog(@"SDK log : chatroom ChatroomMembersListReceived %@",response);
                           
                           [NativeKeys getLogOType:LOG_TYPE_CHATROOM ForMessage:@"ChatroomMembersListReceived"];
                           [[NSNotificationCenter defaultCenter] postNotificationName:@"com.sdkdemo.logsview.refreshLogs" object:nil];
                           
                       } onAVChatMessageReceived:^(NSDictionary *response) {
                           NSLog(@"SDK log : AudioVideo Group Conference message received = %@",response);
                           
                           
                           /* Please Note that each time you join a chatroom last ten messages are received in onChatroomMessageReceived: OR onAVChatMessageReceived: callback blocks. Thus, AudioVideo Conference call messages have to be handled accordingly. */
                           
                           [[NSNotificationCenter defaultCenter] postNotificationName:@"com.sdkdemo.avconferencemessagenotifier" object:nil];
                           
                       } failure:^(NSError *error) {
                           NSLog(@"SDK log : chatroom subscribe error %@",error);
                           
                           [NativeKeys getLogOType:LOG_TYPE_CHATROOM ForMessage:@"Subscribe error"];
                           [[NSNotificationCenter defaultCenter] postNotificationName:@"com.sdkdemo.logsview.refreshLogs" object:nil];
                       }];
}
-(void)viewWillAppear:(BOOL)animated{
  

     _updatePostTextview.translatesAutoresizingMaskIntoConstraints = YES;
       _uploadCollectionView.translatesAutoresizingMaskIntoConstraints = YES;
    _suggestionView.translatesAutoresizingMaskIntoConstraints = YES;
       _baseView.translatesAutoresizingMaskIntoConstraints = YES;
    _postTbl.translatesAutoresizingMaskIntoConstraints=YES;
    _topView.translatesAutoresizingMaskIntoConstraints=YES;
       [self.navigationController setNavigationBarHidden:NO];

    self.navigationItem.title = @"Home";
    [self.navigationController.navigationBar setBottomBorderColor:[UIColor colorWithRed:74/255.0 green:180/255.0 blue:85.0/255.0 alpha:1] height:1];
    if(!isImageScroller){
        [self getPostList:_startNo :_limit];
        
        [self performSelector:@selector(getlistofpeoplemayknow) withObject:nil afterDelay:2.0];
        
        //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
       

        
    }else{
        isImageScroller=NO;
    }
    //    });
     [self commentChatSetup];
    [super viewWillAppear:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:YES];
}
- (void) dealloc
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[super dealloc];
}

-(void)commentChatSetup{
    /* Subscribe to One On One Chat ONLY AFTER successful login.  Set mode argument to YES if you want to strip html elements */
    [cometChat getOnlineUsersWithResponse:^(NSDictionary *response) {
      //  NSLog(@"SDK Log online users : %@",response);
        
        /* Update buddylist table */
        NSMutableArray *buddyList = [[NSMutableArray alloc] init];
        NSArray *users = response.allKeys;
        
        for (int i = 0 ; i < [users count]; i++) {
            
            if ([response objectForKey:[users objectAtIndex:i]]) {
                [buddyList addObject:[response objectForKey:[users objectAtIndex:i]]];
            }
        }
        
        
        /* Store buddyList in userdefaults */
        //[[NSUserDefaults standardUserDefaults] setObject:buddyList forKey:BUDDY_LIST];
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:buddyList] forKey:BUDDY_LIST];
        
        /* Send notification to OneOnOneView to refresh buddyList */
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.inscripts.oneononeview.refreshBuddyList" object:nil];
        
        
    } failure:^(NSError *error) {
        NSLog(@"SDK Log online users error: %@",error);
    }];//    OneOnOneViewController *buddyListViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"oneononeviewcontroller"];
//    [self.navigationController pushViewController:buddyListViewController animated:YES];
//    buddyListViewController = nil;

   
}
-(void)viewDidLayoutSubviews{
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        [self ipad];
    }
    else{
        [self iphone];
    }
    

}
-(void)iphone{
    [_suggestionView setHidden:YES];
    NSLog(@"device is iphone");
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    [_topView setFrame:CGRectMake(0, 64, screenWidth, 220)];
    [_updatePostTextview setFrame:CGRectMake(12, _updatelbl.frame.origin.y+_updatelbl.frame.size.height+20, screenWidth-20, 100)];
   // [_updatePostTextview setBackgroundColor:[UIColor redColor]];
    if(_uploadCollectionView.hidden){
        [_baseView setFrame:CGRectMake(12, _updatePostTextview.frame.origin.y+_updatePostTextview.frame.size.height, screenWidth-20, 50)];
        
//        [_postTbl setBackgroundColor:[UIColor orangeColor]];
    }
    else{
         [_topView setFrame:CGRectMake(0, 64, screenWidth, 350)];
         [_uploadCollectionView setFrame:CGRectMake(12, _updatePostTextview.frame.origin.y+_updatePostTextview.frame.size.height, screenWidth-20, 130)];
      //  [_uploadCollectionView setBackgroundColor:[UIColor greenColor]];
        [_baseView setFrame:CGRectMake(12, _uploadCollectionView.frame.origin.y+_uploadCollectionView.frame.size.height, screenWidth-20, 50)];
        
    }
    
    
    
    if(_suggestionView.hidden){
        [_postTbl setFrame:CGRectMake(12, _topView.frame.origin.y+_topView.frame.size.height, screenWidth-20, screenHeight- (_topView.frame.origin.y+_topView.frame.size.height+50))];
    }
    else{
        [_suggestionView setFrame:CGRectMake(12, _topView.frame.origin.y+_topView.frame.size.height+10, screenWidth-20, 175)];
        
        //[_peopleyouknowCollectionView setBackgroundColor:[UIColor orangeColor]];
        
        [_postTbl setFrame:CGRectMake(12, _suggestionView.frame.origin.y+_suggestionView.frame.size.height, screenWidth-20, screenHeight- (_topView.frame.origin.y+_topView.frame.size.height+_suggestionView.frame.size.height+50))];
    }
    
}
-(void)ipad{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    [_topView setFrame:CGRectMake(0, 64, screenWidth, 220)];
    [_updatePostTextview setFrame:CGRectMake(12, _updatelbl.frame.origin.y+_updatelbl.frame.size.height+20, screenWidth-20, 100)];
    // [_updatePostTextview setBackgroundColor:[UIColor redColor]];
    if(_uploadCollectionView.hidden){
        [_baseView setFrame:CGRectMake(12, _updatePostTextview.frame.origin.y+_updatePostTextview.frame.size.height, screenWidth-20, 50)];
        
        //        [_postTbl setBackgroundColor:[UIColor orangeColor]];
    }
    else{
        [_topView setFrame:CGRectMake(0, 64, screenWidth, 350)];
        [_uploadCollectionView setFrame:CGRectMake(12, _updatePostTextview.frame.origin.y+_updatePostTextview.frame.size.height, screenWidth-20, 130)];
        //  [_uploadCollectionView setBackgroundColor:[UIColor greenColor]];
        [_baseView setFrame:CGRectMake(12, _uploadCollectionView.frame.origin.y+_uploadCollectionView.frame.size.height, screenWidth-20, 50)];
        
    }
    if(_suggestionView.hidden){
        [_postTbl setFrame:CGRectMake(12, _topView.frame.origin.y+_topView.frame.size.height, screenWidth-20, screenHeight- (_topView.frame.origin.y+_topView.frame.size.height+50))];
    }
    else{
        [_suggestionView setFrame:CGRectMake(12, _topView.frame.origin.y+_topView.frame.size.height+10, screenWidth-20, 200)];
        
      //  [_peopleyouknowCollectionView setBackgroundColor:[UIColor orangeColor]];
        
        [_postTbl setFrame:CGRectMake(12, _suggestionView.frame.origin.y+_suggestionView.frame.size.height, screenWidth-20, screenHeight- (_topView.frame.origin.y+_topView.frame.size.height+_suggestionView.frame.size.height+50))];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)addPhotoMethod:(id)sender {
   
    
    

    UIAlertController * view=   [UIAlertController
                                 alertControllerWithTitle:App_title
                                 message:@"Select you Choice"
                                 preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* camera = [UIAlertAction
                         actionWithTitle:@"Camera"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             //Do some thing here
                             [self clickPhoto];
                             [view dismissViewControllerAnimated:YES completion:nil];
                             
                         }];
    UIAlertAction* gallery = [UIAlertAction
                             actionWithTitle:@"Gallery"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [self launchGMImagePicker:nil];
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 
                                 [view dismissViewControllerAnimated:YES completion:nil];
                                 
                             }];

    
    
    [view addAction:camera];
    [view addAction:gallery];
    [view addAction:cancel];
    [view setModalPresentationStyle:UIModalPresentationNone];
    
    UIPopoverPresentationController *popPresenter = [view
                                                     popoverPresentationController];
    popPresenter.sourceView = _gmImagePickerButton;
    popPresenter.sourceRect = _gmImagePickerButton.bounds;//CGRectMake(0, self.view.frame.size.height-250, self.view.frame.size.width, 250);
    [self presentViewController:view animated:YES completion:nil];

}
-(void)clickPhoto{
    if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:App_title
                                                            message:@"Camera not found on this device."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        return;
    }

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
    
}

#pragma camera delegate methods
-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    
    // Access the uncropped image from info dictionary
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    // Save image
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    [self dismissViewControllerAnimated:picker completion:nil];
}
- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertView *alert;
    
    // Unable to save the image
    if (error)
        alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
    else // All is well
        
    [self fetchlastImage];
    
}
-(void)fetchlastImage{
    //NSArray *img;
    
    PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
    fetchOptions.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"creationDate" ascending:YES]];
    PHFetchResult *fetchResult = [PHAsset fetchAssetsWithMediaType:PHAssetMediaTypeImage options:fetchOptions];
    PHAsset *lastAsset = [fetchResult lastObject];
   // imgArray=lastAsset;
   // img =[NSArray arrayWithObjects:lastAsset, nil];
    [imgArray addObject:lastAsset];
    
     [_uploadCollectionView setHidden:NO];
     [self viewDidLayoutSubviews];
    [_uploadCollectionView reloadData];
  
    
   // lastAsset=nil;
//    [[PHImageManager defaultManager] requestImageForAsset:lastAsset
//                                               targetSize:CGSizeMake(120  , 120)
//                                              contentMode:PHImageContentModeAspectFill
//                                                  options:PHImageRequestOptionsVersionCurrent
//                                            resultHandler:^(UIImage *result, NSDictionary *info) {
//                                                
//                                                dispatch_async(dispatch_get_main_queue(), ^{
//                                                    
//                                                    [[self photoLibraryButton] setImage:result forState:UIControlStateNormal];
//                                                    
//                                                });
                                           // }];

}
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
     [self dismissViewControllerAnimated:picker completion:nil];
}
- (IBAction)launchGMImagePicker:(id)sender
{
   // NSLog(@"imgArray is %@",imgArray);
     [imgArray removeAllObjects];
    [postImages removeAllObjects];
   
        GMImagePickerController *picker = [[GMImagePickerController alloc] init];
    picker.delegate = self;
   // picker.title = @"Custom title";
   // picker.customNavigationBarPrompt = @"Custom helper message!";
    picker.colsInPortrait = 3;
    picker.colsInLandscape = 5;
    picker.minimumInteritemSpacing = 2.0;
    picker.modalPresentationStyle = UIModalPresentationPopover;
    
    UIPopoverPresentationController *popPC = picker.popoverPresentationController;
    popPC.permittedArrowDirections = UIPopoverArrowDirectionAny;
    popPC.sourceView = _gmImagePickerButton;
    popPC.sourceRect = _gmImagePickerButton.bounds;
    [self presentViewController:picker animated:YES completion:nil];
    
  //  [self showViewController:picker sender:nil];
}
#pragma mark - GMImagePickerControllerDelegate

- (void)assetsPickerController:(GMImagePickerController *)picker didFinishPickingAssets:(NSMutableArray *)assetArray
{
    imgArray=assetArray;
    
    if([imgArray count]==0){
        [_uploadCollectionView setHidden:YES];
    }
    else{
         [_uploadCollectionView setHidden:NO];
    }
    
    
    [_uploadCollectionView reloadData];
    
    [picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    
   
    

    [self viewDidLayoutSubviews];
    NSLog(@"GMImagePicker: User ended picking assets. Number of selected items is: %lu", (unsigned long)assetArray.count);
}

//Optional implementation:
-(void)assetsPickerControllerDidCancel:(GMImagePickerController *)picker
{
    NSLog(@"GMImagePicker: User pressed cancel button");
}

-(void)removeFromGroup:(id)sender

{
    
   // NSLog(@"sender tag ===%ld",(long)[sender tag]);
    
    //PHAsset *asset = [arr
    if([imgArray count]==0){
        [_uploadCollectionView setHidden:YES];
    }
    else{
    [imgArray removeObjectAtIndex:[sender tag]];
        //NSLog(@"post image array is %@",postImages);
    [postImages removeAllObjects];
       // NSLog(@"post image array2 is %@",postImages);
        if([imgArray count]==0){
            [_uploadCollectionView setHidden:YES];
            [self viewDidLayoutSubviews];
        }
   
    [_uploadCollectionView reloadData];
    }
    //  NSLog(@"image array for delete is %@",asset);
    
    
    
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if(collectionView == _peopleyouknowCollectionView){
        return _items.items.count;
    }
    else{
   
    return [imgArray count];
}
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView ==_peopleyouknowCollectionView){
        PeopleYouKnowCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"PeopleYouKnowCell" forIndexPath:indexPath];
        
        cell.backgroundColor=[UIColor whiteColor];
        cell.imgView.image =[UIImage imageNamed:@"pic"];
        PeoplemayknowModelKeys *post=_items.items[indexPath.row];
        NSURL *url = [NSURL URLWithString:post.u_profile_photo];
        cell.suggestionLbl.text=[Utils fullName:post.u_first_name :post.u_last_name];
        [cell.imgView sd_setImageWithURL:url
                          placeholderImage:[UIImage imageNamed:@"noimage"]];
        cell.addFriendBtn.tag=indexPath.row;
        [ cell.addFriendBtn addTarget:self
         
                             action:@selector(addFriend:)
         
                   forControlEvents:UIControlEventTouchDown];
        
        return cell;

    }
    else{
    imgCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"imgCollectionViewCell" forIndexPath:indexPath];
    
    cell.backgroundColor=[UIColor whiteColor];
    
    
    
    cell.crossBtn.tag=indexPath.row;
   [ cell.crossBtn addTarget:self
    
action:@selector(removeFromGroup:)
    
forControlEvents:UIControlEventTouchDown];
    
    
    
    
    PHAsset *asset =[imgArray objectAtIndex:indexPath.row];
    
    
    
   // NSLog(@"image array is %@",asset);
    
    [[PHImageManager defaultManager]
     
     requestImageForAsset:asset
     
     targetSize:CGSizeMake(120  , 120)
     
     contentMode:PHImageContentModeAspectFit
     
     options:nil
     
     resultHandler:^(UIImage *result, NSDictionary *info) {
        
         cell.imgView.image = result;
         
        
     }];
    
       
   
       return cell;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView==_peopleyouknowCollectionView){
        return CGSizeMake(100, 150);
    }
    return CGSizeMake(120, 120);
}

-(NSMutableArray *)makeArrayforupload{
    NSLog(@"images are in progress");
    PHAsset *asset ;//=[imgArray objectAtIndex:indexPath.row];
    
    for (asset in imgArray) {
        
    
    [[PHImageManager defaultManager] requestImageDataForAsset:asset options:nil resultHandler:^(NSData * _Nullable imageData, NSString * _Nullable dataUTI, UIImageOrientation orientation, NSDictionary * _Nullable info) {
        
        UIImage *img= [Utils compressImage:[UIImage imageWithData:imageData]];
        NSData *imageData2 = UIImagePNGRepresentation(img);
        //or
        //
        NSString *b64EncStr = [Base64 encode:imageData2];
        
        NSDictionary *aDic= [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSString stringWithFormat:@"%@",b64EncStr] ,
                             @"image", nil];
        
        // sadly result is not a squared image
        [postImages addObject:aDic];
        NSInteger imageSize = imageData2.length;
        //NSLog(@"Images for upload%lu and size is %@",(unsigned long)[postImages count],imageData2);
        NSLog(@"SIZE OF IMAGE: %.2f mb", (float)imageSize/1024/1024);
        img=nil;
        imageData2=nil;
        //aDic=nil;
        
    }];
        }
    return postImages;

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (imageArray == nil || [imageArray count] == 0 || [imageArray containsObject:@""]){
        return 205;
    }
    else{
        return 455;
    }

    //[((NSNumber*)[cardSizeArray objectAtIndex:indexPath.row])intValue];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning you're going to want to change this
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning you're going to want to change this
    return _feed.feed.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 2; // you can have your own choice, of course
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *MyIdentifier = @"DashboardTableViewCell";
    
    DashboardTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
   
    cell.delegate=self;
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[DashboardTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DashboardTableViewCell"];
    }
  
   
PostModel *post = _feed.feed[indexPath.row];
   
   // NSLog(@"Post array is %@",post);
    
//    if (indexPath.row == [_feed.feed count] - 1)
//    {
//        [self getPostList];
//    }
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    });
    NSDate *date = [dateFormatter dateFromString:post.created];
    cell.selectedCell=indexPath;
   // [Utils fullName:post.u_first_name :post.u_last_name];
       cell.headerLbl.text=[Utils fullName:post.u_first_name :post.u_last_name];//[post valueForKey:@"u_first_name"];
    cell.dateLbl.text=[NSString stringWithFormat:@"%@ %@", [date longRelativeDateString], [date timeString]];//[post valueForKey:@"created"];
    cell.descLbl.text=post.content;//[post valueForKey:@"content"];
    cell.likeLbl.text=post.total_like;//[post valueForKey:@"total_like"];
    cell.commentLbl.text=post.total_comments;//[post valueForKey:@"total_comments"];
    cell.wallid=post.wall_id;
   
    NSURL *url = [NSURL URLWithString:post.u_profile_photo];
    [cell.imageView sd_setImageWithURL:url
                     placeholderImage:[UIImage imageNamed:@"noimage"]];
 
    
    //postImagesfeed *pfeed=post.post_image_list;
   // imageArray=[NSArray arrayWithObjects:post.post_image_list, nil];
//
    if([post.post_image_list count]==0){
        imageArray=nil;//[NSArray arrayWithObjects:@"", nil];
    }
    else{
    imageArray=[NSArray arrayWithObjects:post.post_image_list, nil];
        
   
    }
     cell.imagesName=post.post_image_list;
    cell.commentBtn.tag=indexPath.row;
    [ cell.commentBtn addTarget:self
     
                       action:@selector(viewComment:)
     
             forControlEvents:UIControlEventTouchDown];
    cell.likeBtn.tag=indexPath.row;
    [ cell.likeBtn addTarget:self
     
                         action:@selector(likePost:)
     
               forControlEvents:UIControlEventTouchDown];

    
    //%%% I made the cards pseudo dynamic, so I'm asking the cards to change their frames depending on the height of the cell
    //cell.cardView.frame = CGRectMake(10, 5, 300, [((NSNumber*)[cardSizeArray objectAtIndex:indexPath.row])intValue]-10);
    
    return cell;
}
-(void)likePost:(id)sender {
    
   
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //{"userid":"233","wall_id":"7"}
    PostModel *post = _feed.feed[[sender tag]];
   
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]     forKey:@"u_id"];
   
    [postDict setValue:[NSString stringWithFormat:@"%@",post.wall_id] forKey:@"wall_id"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlLikePost parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Utils showAlertwithMessage:status.message];
        }
        else{
            
            PostModel *p1=_feed.feed[[sender tag]];
           //[responseObject objectForKey:@"total_like"];
            
        
           // NSLog(@"loggedin Userid  is %@",totalLikeOld) ;
            p1.total_like=[responseObject objectForKey:@"total_like"];
            
            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
            //            NSArray *indexPaths = [[NSArray alloc] initWithObjects:path, nil];
[_postTbl reloadData];
            [_postTbl scrollToRowAtIndexPath:rowToReload atScrollPosition:UITableViewScrollPositionNone animated:NO];
            
          //  [_feed.feed replaceObjectAtIndex:[sender tag] withObject:p1];
          // NSLog(@"New Updated feed is %@",[_feed.feed  objectAtIndex:[sender tag]]) ;
//  NSIndexPath *path=[NSIndexPath indexPathForRow:[sender tag] inSection:0];
////            
////         //   UITableViewCell* cell = [_postTbl cellForRowAtIndexPath:path];
////             NSLog(@"loggedin Userid  is %lu",[sender tag]) ;
////            NSIndexPath* rowToReload = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
//            NSArray *indexPaths = [[NSArray alloc] initWithObjects:path, nil];
//           [_postTbl reloadRowsAtIndexPaths:indexPaths
//                             withRowAnimation:UITableViewRowAnimationRight];
////            
            
          //  [_postTbl scrollToRowAtIndexPath:rowToReload atScrollPosition:[sender tag] animated:NO];
            //You can change animation option.
            //[_postTbl endUpdates];//            NSArray* rowsToReload = [NSArray arrayWithObjects:rowToReload, nil];
            
            //[Utils showAlertwithMessage:postListArray];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            //        [imgArray removeAllObjects];
            //        [postImages removeAllObjects];
            //        [_uploadCollectionView setHidden:YES];
            //        [self viewDidLayoutSubviews];
        }
    }
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
              [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
    
    
}
-(void)addFriend:(id)sender{
    PostModel *post = _items.items[[sender tag]];
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]  forKey:@"user_id"];
    [postDict setValue:post.u_id  forKey:@"profile_id"];
    [postDict setValue:@"SEND"  forKey:@"status_name"];
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlChangeConnectionStatus parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
      //  NSLog(@"Request Response is %@",responseObject);
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            //[Utils showAlertwithMessage:status.message];
        }
        else{
             [_items.items removeObjectAtIndex:[sender tag]];
            if([_items.items count]==0){
                [_suggestionView setHidden:YES];
            }
            else{
                [_suggestionView setHidden:NO];
            }
            
            
            
            [_peopleyouknowCollectionView reloadData];
            [self viewDidLayoutSubviews];

           
            //[_peopleyouknowCollectionView reloadData];
            
            //            NSString *connectionStatus=_selectedFriend.connection_status;
            //            NSString *connectionStatusBy=_selectedFriend.connection_status_by;
            
            
            
            
           
            
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }
    }
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
              [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
    

}
-(void)viewComment:(id)sender

{
      PostModel *post = _feed.feed[[sender tag]];
       commentVC =[MainStoryBoard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
    commentVC.delegate=self;
    commentVC.wallId=post.wall_id;
    [self presentViewController:commentVC animated:YES completion:nil];

    
  //  NSLog(@"Comment tag ===%ld",(long)[sender tag]);
}
-(void)sendChangedPaswdToServer:(CommentsViewController *)aSecondDetailViewController{
    
  //  NSLog(@"new paswd sent to server. Dismiss the poup view");
    [self dismissViewControllerAnimated:commentVC completion:nil];
    [self getPostList:_startNo :_limit];
    
    
}

- (void)cancelButtonClicked:(CommentsViewController *)aSecondDetailViewController
{
    
   [self dismissViewControllerAnimated:commentVC completion:nil];
    NSLog(@"closed now");
    
}
-(void)getPostList:(NSInteger)startno :(NSInteger)limit{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //{"userid":"125","limitfrom":"0","limitto":"5"}
  //  NSLog(@"loggedin Userid  is %@",[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]) ;
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]     forKey:@"u_id"];
    [postDict setValue:[NSString stringWithFormat:@"%ld",(long)_startNo] forKey:@"limitfrom"];
    [postDict setValue:[NSString stringWithFormat:@"%ld",(long)_limit] forKey:@"limitto"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlPostList parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Utils showAlertwithMessage:status.message];
        }
        else{
           
            _feed=[[postFeed alloc]initWithDictionary:responseObject error:&err];
            
            
            
          
      //  NSLog(@"JSON: %@", _feed.feed);
            [_postTbl reloadData];
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

        }
    }
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
              
              [Utils showAlertwithMessage:@"Oops,Something went wrong"];
              [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
    

}
-(void)apicall{
      NSLog(@"Count final array for upload is %lu",(unsigned long)[postImages count]);
    if([_updatePostTextview.text isEqualToString:@"What's in your mind."] && [postImages count]==0){
        //[MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        return;
    }
    else{
        NSString *updateText;
        if([_updatePostTextview.text isEqualToString:@"What's in your mind."]){
            //  [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            updateText=@"";
            //return;
        }
        else{
            updateText=_updatePostTextview.text;
        }
        NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
        [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]     forKey:@"u_id"];
        [postDict setValue:updateText forKey:@"content"];
        [postDict setValue:postImages forKey:@"upload_image"];
        
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
       // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [manager POST:kBaseUrlPostUpload parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
            NSError* err = nil;
            
            
          //  NSLog(@"JSON: %@", responseObject);
            
            
            
            
            
            
          //  NSLog(@"Original feeds: %@", _feed.feed);
            
            
            if([_feed.feed count]==0){
                _feed=[[postFeed alloc]initWithDictionary:responseObject error:&err];
            }
            else{
                // _feed=[[postFeed alloc]initWithDictionary:responseObject error:&err];
                postFeed *_new_feed=[[postFeed alloc]initWithDictionary:responseObject error:&err];
                
                
                PostModel *post = [_new_feed.feed objectAtIndex:0];
                
                
                [_feed.feed insertObject:post atIndex:0];
            }
            NSLog(@"New Feed: %@", _feed.feed);
            dispatch_async(dispatch_get_main_queue(), ^{
                // code here
                [MBProgressHUD hideHUDForView:self.view animated:YES];

                _updatePostTextview.text =nil;
                [imgArray removeAllObjects];
                [postImages removeAllObjects];
                [_postTbl reloadData];
                [_uploadCollectionView setHidden:YES];
                [self viewDidLayoutSubviews];
            });

            
           // [_postTbl reloadData];
            //[Utils showAlertwithMessage:postListArray];
         //   [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            

//            _updatePostTextview.text =nil;
//            [imgArray removeAllObjects];
//            [postImages removeAllObjects];
//            [_postTbl reloadData];
//            [_uploadCollectionView setHidden:YES];
//            [self viewDidLayoutSubviews];
//            [_uploadCollectionView setHidden:YES];
//            [self viewDidLayoutSubviews];
            
            
            //        {
            //            content = "hiii india ";
            //            created = "2015-12-04 01:10:22";
            //            error = False;
            //            image = "wall_01449213022.";
            //            "image_url" = "http://magnon.co.in/uploads/wall_image";
            //            message = "Wall post posted successfully.";
            //            "sender_id" = 125;
            //            "wall_id" = 40;
            //        }
            
            
            //  [Utils showAlertwithMessage:[responseObject valueForKey:@"message"]];
        }
         
         
         
              failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  NSLog(@"fail>>>>>%@",error);
               //   [MBProgressHUD hideHUDForView:self.view animated:YES];
                  
              }];
    }

}
- (IBAction)postUploadMethod:(id)sender {
    [self.view endEditing:YES];
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    
    // create a group
    dispatch_group_t group = dispatch_group_create();
    
    // pair a dispatch_group_enter for each dispatch_group_leave
    dispatch_group_enter(group);     // pair 1 enter
    [self computeInBackground:1 completion:^{
        NSLog(@"1 done");
        [self makeArrayforupload];
        dispatch_group_leave(group); // pair 1 leave
    }];
    
    // again... (and again...)
    dispatch_group_enter(group);     // pair 2 enter
    [self computeInBackground:2 completion:^{
        NSLog(@"2 done");
        [self apicall];
        dispatch_group_leave(group); // pair 2 leave
    }];
    
    // Next, setup the code to execute after all the paired enter/leave calls.
    //
    // Option 1: Get a notification on a block that will be scheduled on the specified queue:
    dispatch_group_notify(group, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        
                NSLog(@"finally!");
    });
    
   }

- (void)computeInBackground:(int)no completion:(void (^)(void))block {
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSLog(@"%d starting", no);
        
        sleep(no*2);
        block();
    });
}
-(void)getlistofpeoplemayknow{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]     forKey:@"user_id"];
    
    
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [manager POST:kBaseUrlPeopleyoumayknow parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // NSLog(@"People you may know response list: %@", responseObject);
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            [_items.items removeAllObjects];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            //[Utils showAlertwithMessage:status.message];
        }
        else{
            
            _items=[[peoplemayKnowModel alloc]initWithDictionary:responseObject error:&err];
           
            if([_items.items count]==0){
                [_suggestionView setHidden:YES];
            }
            else{
                [_suggestionView setHidden:NO];
            }
            
            
           
            [_peopleyouknowCollectionView reloadData];
             [self viewDidLayoutSubviews];
            // self.searchFriendList=_items.items;
            
          //   NSLog(@"People you may know response list: %@", _items);
          
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }
    }
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
              [Utils showAlertwithMessage:@"Oops ,Something went wrong!"];
              [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
    


}



#pragma TextView delegate methods
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchesBegan:withEvent:");
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"textViewShouldBeginEditing:");
    textView.textColor=[UIColor blackColor];
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    NSLog(@"textViewDidBeginEditing:");
    if ([textView.text isEqualToString:@"What's in your mind."]) {
        textView.text=nil;
    }
    else{
        textView.textColor=[UIColor blackColor];
    }

    //textView.backgroundColor = [UIColor greenColor];
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    NSLog(@"textViewShouldEndEditing:");
    textView.backgroundColor = [UIColor whiteColor];
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"textViewDidEndEditing:");
    if([textView.text isEqualToString:@""]){
        textView.text=@"What's in your mind.";
        textView.textColor=[UIColor grayColor];
    }
    else{
        textView.textColor=[UIColor blackColor];
    }
}

@end
