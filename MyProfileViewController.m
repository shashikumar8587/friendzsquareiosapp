//
//  MyProfileViewController.m
//  friendsquare
//
//  Created by magnon on 18/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "MyProfileViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "Status.h"
#import "Utils.h"
#import "Base64.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "NSDate+Additions.h"
#import "UINavigationBar+navigationBar.h"


@implementation MyProfileViewController
- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return YES;
}
- (void)viewDidLoad {
    
        [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
    
    self.navigationItem.title = @"My Profile";
    [self.navigationController.navigationBar setBottomBorderColor:[UIColor colorWithRed:74/255.0 green:180/255.0 blue:85.0/255.0 alpha:1] height:1];
    [self myProfile];
    
}
-(void)myProfile{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    
    [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid] forKey:@"user_id"];
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlGetUserProfile parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"User profile is %@",responseObject);
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            //[Utils showAlertwithMessage:status.message];
        }
        else{
            
            getUserDetail = [[getUserDetailModel alloc] initWithDictionary:responseObject error:&err];
            NSLog(@"Name is User %@",getUserDetail);
            [self setUpview];
            //[Utils showAlertwithMessage:postListArray];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }
    }
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
              [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
    
    
    
}
-(void)setUpview{
    
    NSString *imageUrl=getUserDetail.u_profile_photo;
    NSURL *url = [NSURL URLWithString:imageUrl];
    
    // download the image asynchronously
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded) {
                    // change the image in the cell
                    _profilePic.image = image;
                    
                    // cache the image for use later (when scrolling up)
                    //img.image = image;
                }
            }];
        });
    });
    
    
    _fullName.text=[Utils fullName:getUserDetail.u_first_name :getUserDetail.u_last_name];//[post valueForKey:@"u_first_name"];
    
    _designationLbl.text=getUserDetail.u_current_profession;
    _fullAddress.text=[Utils completeaddress:getUserDetail.u_state :getUserDetail.u_country];
    _myFriends.text=getUserDetail.total_connection;
    _gender.text=getUserDetail.u_gender;
    _phonenoLbl.text=getUserDetail.u_phone_number;
    _emailLbl.text=getUserDetail.u_email;
}
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)updateProfile:(id)sender {
}
@end
