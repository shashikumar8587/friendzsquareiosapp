//
//  RegisterViewController.h
//  friendsquare
//
//  Created by magnon on 26/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate>{
    
}
@property (weak, nonatomic) IBOutlet UITextField *firstNameTxt;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTxt;
- (IBAction)alreadyAccount:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UITextField *pwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *conPwdTxt;
@property (weak, nonatomic) IBOutlet UITextField *PhoneTxt;
- (IBAction)signUp:(id)sender;

@end
