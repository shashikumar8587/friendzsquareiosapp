//
//  getUserDetailModel.h
//  friendsquare
//
//  Created by magnon on 18/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//


#import <JSONModel/JSONModel.h>
@protocol getUserDetailModel
@end
@interface getUserDetailModel : JSONModel
@property(nonatomic,copy)NSString *total_connection;
@property(nonatomic,copy)NSString *u_activation_code;
@property(nonatomic,copy)NSString *u_activation_date;


@property(nonatomic,copy)NSString *u_registered_trough;
@property(nonatomic,copy)NSString *u_registered_platform;
@property(nonatomic,copy)NSString *u_reason_for_change_status;
@property(nonatomic,copy)NSString *u_linkedin_id;
@property(nonatomic,copy)NSString *u_last_login;
@property(nonatomic,copy)NSString *u_ip_address;
@property(nonatomic,copy)NSString *u_guid;
@property(nonatomic,copy)NSString *u_google_id;
@property(nonatomic,copy)NSString *u_fb_id;
@property(nonatomic,copy)NSString *u_added_date;
@property(nonatomic,copy)NSString *u_activation_status;
@property(nonatomic,copy)NSString *u_status;

@property(nonatomic,copy)NSString *u_display_name;
@property(nonatomic,copy)NSString *u_id;
@property(nonatomic,copy)NSString *u_first_name;
@property(nonatomic,copy)NSString *u_last_name;
@property(nonatomic,copy)NSString *u_email;
@property(nonatomic,copy)NSString *u_gender;
@property(nonatomic,copy)NSString *u_personal_visibility;
@property(nonatomic,copy)NSString *u_marital_status;
@property(nonatomic,copy)NSString *u_profile_photo;
@property(nonatomic,copy)NSString *u_country;
@property(nonatomic,copy)NSString *u_state;
@property(nonatomic,copy)NSString *u_city;
@property(nonatomic,copy)NSString *u_zip_code;
@property(nonatomic,copy)NSString *u_phone_number;
@property(nonatomic,copy)NSString *u_qualification_visibility;
@property(nonatomic,copy)NSString *u_education_level;
@property(nonatomic,copy)NSString *u_college_specialization;
@property(nonatomic,copy)NSString *u_graduate_year;
@property(nonatomic,copy)NSString *u_study_status;
@property(nonatomic,copy)NSString *u_city_establishment;
@property(nonatomic,copy)NSString *u_country_establishment;
@property(nonatomic,copy)NSString *u_education_establishment;
@property(nonatomic,copy)NSString *u_professional_visibility;
@property(nonatomic,copy)NSString *u_current_profession;
@property(nonatomic,copy)NSString *u_sub_profession;
@property(nonatomic,copy)NSString *u_current_position;
@property(nonatomic,copy)NSString *u_doc;

@end





