//
//  ConnectionTableViewCell.h
//  friendsquare
//
//  Created by magnon on 20/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ConnectionTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *headerLbl;
@property (weak, nonatomic) IBOutlet UILabel *subLbl;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UIButton *requestStatusLbl;

@end
