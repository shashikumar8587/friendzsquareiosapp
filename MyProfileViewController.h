//
//  MyProfileViewController.h
//  friendsquare
//
//  Created by magnon on 18/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "getUserDetailModel.h"

@interface MyProfileViewController : UIViewController{
    getUserDetailModel * getUserDetail;
}
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UILabel *fullName;
@property (weak, nonatomic) IBOutlet UILabel *designationLbl;
@property (weak, nonatomic) IBOutlet UILabel *fullAddress;
@property (weak, nonatomic) IBOutlet UILabel *myFriends;
- (IBAction)updateProfile:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *gender;
@property (weak, nonatomic) IBOutlet UILabel *emailLbl;
@property (weak, nonatomic) IBOutlet UILabel *phonenoLbl;

@end
