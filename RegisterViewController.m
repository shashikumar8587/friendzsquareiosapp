//
//  RegisterViewController.m
//  friendsquare
//
//  Created by magnon on 26/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"

#import "AFNetworking.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "Status.h"
#import "Utils.h"

@interface RegisterViewController (){
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpacing;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [self.navigationController setNavigationBarHidden:YES];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*///{"fullname":"sunil yadav","email":"sunil@gmail.com","password":"123456"}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)alreadyAccount:(id)sender {
    LoginViewController *login=(LoginViewController *)[MainStoryBoard instantiateViewControllerWithIdentifier:@"Login"];
    // login.user=user;
    [self.navigationController pushViewController:login animated:NO];
}
- (IBAction)signUp:(id)sender {
    
    if ([self checkValidation]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
        [userInfo setValue:_firstNameTxt.text forKey:@"firstname"];
         [userInfo setValue:_lastNameTxt.text forKey:@"lastname"];
      
        [userInfo setValue:[_emailTxt.text lowercaseString]  forKey:@"email"];
         [userInfo setValue:_PhoneTxt.text forKey:@"phone"];
        [userInfo setValue:_pwdTxt.text forKey:@"password"];
        
        
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
        
        
        [manager POST:kBaseUrlRegister parameters:userInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
           // [MBProgressHUD hideHUDForView:self.view animated:YES];
            NSLog(@"JSON: %@", responseObject);
            
            NSError* err = nil;
            Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
            
            if([status.error isEqualToString:@"True"]){
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [Utils showAlertwithMessage:status.message];
                
//                LoginViewController *login=(LoginViewController *)[MainStoryBoard instantiateViewControllerWithIdentifier:@"Login"];
//                // login.user=user;
//                [self.navigationController pushViewController:login animated:YES];
                
            }
            else{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [Utils showAlertwithMessage:status.message];
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
        }];
    }
 
}
#pragma textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    self.verticalSpacing.constant = -90;
    
    [UIView animateWithDuration:.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    self.verticalSpacing.constant = -0;
    
    [UIView animateWithDuration:.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    return YES;
}
#pragma validate textfields
- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

-(BOOL) checkValidation
{
    if([_firstNameTxt.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter your first Name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    if([_lastNameTxt.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter your last Name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    if (![self validateEmail:_emailTxt.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter a valid email address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
   
     if([_PhoneTxt.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter your phone number." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    if([_pwdTxt.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter your password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
   
    if (![_pwdTxt.text isEqualToString:_conPwdTxt.text]) {
        
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Password and confirm password must be same." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    return YES;
}

@end
