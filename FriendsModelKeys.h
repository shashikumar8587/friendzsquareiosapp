//
//  FriendsModelKeys.h
//  friendsquare
//
//  Created by magnon on 18/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <JSONModel/JSONModel.h>
@protocol FriendsModelKeys
@end
@interface FriendsModelKeys : JSONModel

@property (strong, nonatomic) NSString* connection_status;
@property (strong, nonatomic) NSString* connection_status_by;
@property (strong, nonatomic) NSString* connection_status_name;
@property (strong, nonatomic) NSString* total_connection;


@property (strong, nonatomic) NSString* u_added_date;
@property (strong, nonatomic) NSString* u_city;
@property (strong, nonatomic) NSString* u_college_specialization;
@property (strong, nonatomic) NSString* u_country;
@property (strong, nonatomic) NSString* u_current_profession;
@property (strong, nonatomic) NSString* u_display_name;
@property (strong, nonatomic) NSString* u_education_level;
@property (strong, nonatomic) NSString* u_email;
@property (strong, nonatomic) NSString* u_first_name;
@property (strong, nonatomic) NSString* u_gender;
@property (strong, nonatomic) NSString* u_guid;

@property (strong, nonatomic) NSString* u_id;
@property (strong, nonatomic) NSString* u_last_name;
@property (strong, nonatomic) NSString* u_marital_status;
@property (strong, nonatomic) NSString* u_phone_number;
@property (strong, nonatomic) NSString* u_profile_photo;
@property (strong, nonatomic) NSString* u_state;

@property (strong, nonatomic) NSString* u_zip_code;

@end
