//
//  SearchViewController.m
//  Gastronome
//
//  Created by MIPC-52 on 26/03/14.
//  Copyright (c) 2014 Magnon International. All rights reserved.
//

#import "SearchViewController.h"
#import "AppDelegate.h"

#import "ConnectionTableViewCell.h"
#import "UINavigationBar+navigationBar.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "Status.h"
#import "Utils.h"
#import "Base64.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "NSDate+Additions.h"
#import "ProfileViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController
@synthesize SearchArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    UIButton *tmpSearch=[UIButton buttonWithType:UIButtonTypeCustom];
    [tmpSearch setTitle:@"Cancel" forState:UIControlStateNormal];
    [tmpSearch setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
     [tmpSearch setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
    //[tmpSearch setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    tmpSearch.frame=CGRectMake(0, 0, 72, 32);
    tmpSearch.tag=102;
    [tmpSearch addTarget:self
                  action:@selector(searchBtnclicked)
        forControlEvents:UIControlEventTouchDown];
    
    if([AppDelegate GetAppDelegate].isCancelSearch){
        tmpSearch.hidden=NO;
         [AppDelegate GetAppDelegate].isCancelSearch=NO;
    }
    else{
        tmpSearch.hidden=YES;
    }
    
      
    self.title = @"Search";
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UITextField  *textfieldTxt = [[UITextField alloc]initWithFrame:CGRectMake(0.0f, 100.0f, 320.0f, 31.0f)];
    textfieldTxt.placeholder=@"Search friends.";
   textfieldTxt.backgroundColor = [UIColor whiteColor];
    [textfieldTxt setAutocorrectionType:UITextAutocorrectionTypeNo];
    textfieldTxt.delegate = self;
   textfieldTxt.tag=101;
    textfieldTxt.textColor=[UIColor colorWithRed:155.0/255.00 green:155.0/255.00 blue:155.0/255.00 alpha:1];

    textfieldTxt.font = [UIFont systemFontOfSize:10.0f];
    textfieldTxt.borderStyle = UITextBorderStyleRoundedRect;
    textfieldTxt.clearButtonMode = UITextFieldViewModeWhileEditing;
    textfieldTxt.returnKeyType = UIReturnKeyDone;
   
    textfieldTxt.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    textfieldTxt.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.navigationItem.titleView = textfieldTxt;
    
    
    
    
   
    
    UIBarButtonItem *barBtnSearch=[[UIBarButtonItem alloc]initWithCustomView:tmpSearch];
    self.navigationItem.rightBarButtonItem=barBtnSearch;
    
    
    
    
    
    
    
    
    
    
    
}
-(void)viewWillAppear:(BOOL)animated{
    [self searchFromserver:@""];
    
}
-(void)searchBtnclicked{
    NSLog(@"hiii");
    [self.navigationController popViewControllerAnimated:NO];
    
}
#pragma mark - TextField Delegates

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    //write code to be executed on tap of clear button
    return YES;
}
// This method is called once we click inside the textField
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"Text field did begin editing");
}

// This method is called once we complete editing
-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"Text field ended editing");
    [self searchFromserver:textField.text];
    
   
}

// This method enables or disables the processing of return key
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
//    SearchDetailViewController *dash=[[SearchDetailViewController alloc]initWithNibName:@"SearchDetailViewController" bundle:nil];
//    
//    dash.KeywordFromSearch=textField.text;
//    [self.navigationController pushViewController:dash animated:NO];
    return YES;
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)searchFromserver:(NSString *)str{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    
    [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]     forKey:@"user_id"];
    [postDict setValue:str forKey:@"search_name"];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlSearchFriend parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Search list is %@",responseObject);
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            [_items.items removeAllObjects];
             [MBProgressHUD hideHUDForView:self.view animated:YES];
            //[Utils showAlertwithMessage:status.message];
        }
        else{
            
            _items=[[FriendsModel alloc]initWithDictionary:responseObject error:&err];
            
            
            // self.searchFriendList=_items.items;
            
            // NSLog(@"JSON: %@", _feed.feed);
            _resultCount.text=[NSString stringWithFormat:@"About %@ results found",_items.current_user_count];
             [_connectionTableView reloadData];
            //[Utils showAlertwithMessage:postListArray];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }
    }
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
              [Utils showAlertwithMessage:@"Oops ,Something went wrong!"];
              [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
    
    
}



#pragma mark-- Delegate methods of Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_items.items count];
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"ConnectionCell";
    
    ConnectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[ConnectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:MyIdentifier];
    }
    //cell.imageView.image
    //cell.contentView.backgroundColor=[UIColor clearColor];
    FriendsModelKeys *post = _items.items[indexPath.row];
    
    NSLog(@"Friendlist model array is %@",post);
    
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    });
    
//    UIButton *btn=(UIButton *)[cell.contentView viewWithTag:101];
//    btn.tag=indexPath.row;
//    if([post.connection_status isEqualToString:@"1"]){
//        [btn setTitle:@"Connected" forState:UIControlStateNormal];
//        [btn setUserInteractionEnabled:NO];
//    }
    
    
    if([post.connection_status isEqualToString:@""] || [post.connection_status isEqual:[NSNull null]]){
        
        [cell.requestStatusLbl setTitle:@"Invite Friend" forState:UIControlStateNormal];
        [cell.requestStatusLbl  setUserInteractionEnabled:NO];

    }
     if ([post.connection_status isEqualToString:@"0"]){
        if([post.connection_status_by isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]]){

        [cell.requestStatusLbl setTitle:@"Request Sent" forState:UIControlStateNormal];
        [cell.requestStatusLbl setUserInteractionEnabled:NO];
        }
        else{
            [cell.requestStatusLbl setTitle:@"Request Recieved" forState:UIControlStateNormal];
            [cell.requestStatusLbl setUserInteractionEnabled:NO];
        }

       
    }
     if ([post.connection_status isEqualToString:@"1"]){
        [cell.requestStatusLbl setTitle:@"Connected" forState:UIControlStateNormal];
        [cell.requestStatusLbl setUserInteractionEnabled:NO];

      
    }
    

    
    
    
    
    
    // [Utils fullName:post.u_first_name :post.u_last_name];
    cell.headerLbl.text=[Utils fullName:post.u_first_name :post.u_last_name];//[post valueForKey:@"u_first_name"];
    cell.subLbl.text=[Utils completeaddress:post.u_state :post.u_country];
    NSURL *url = [NSURL URLWithString:post.u_profile_photo];
    cell.imgView.layer.cornerRadius = cell.imgView.frame.size.width/2;
    cell.imgView.clipsToBounds = YES;
    cell.imgView.contentMode = UIViewContentModeScaleAspectFit;
    cell.imgView.backgroundColor = [UIColor whiteColor];
    [cell.imgView sd_setImageWithURL:url
                      placeholderImage:[UIImage imageNamed:@"noimage"]];

    
    return cell;
    
    
    //    }
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    FriendsModelKeys *post = _items.items[indexPath.row];
    ProfileViewController *profile=[MainStoryBoard instantiateViewControllerWithIdentifier: @"ProfileViewController"];
    profile.selectedFriend=post;
    [self.navigationController pushViewController:profile animated:NO];
    
}

@end
