//
//  DashboardTableViewCell.h
//  friendsquare
//
//  Created by magnon on 27/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PostModel.h"
#import "ScrollViewAdd.h"
@class DashboardTableViewCell;
@protocol DashboardTableViewCellDelegate;
//define class, so protocol can see MyClass
@protocol DashboardTableViewCellDelegate <NSObject>   //define delegate protocol
- (void) myClassDelegateMethod: (NSInteger) index :(NSIndexPath *)ind;  //define delegate method to be implemented within another class
@end //end protocol
@interface DashboardTableViewCell : UITableViewCell<UIScrollViewDelegate>{
    //ASScroll *asScroll;
    ScrollViewAdd *scrollView;
   // ImageScroller *imgGenerator;
    NSMutableArray *images;
   // NSArray * imagesName ;
}
@property (strong,nonatomic)NSIndexPath *selectedCell;
//@property(retain,nonatomic)ImageScroller *imgGenerator;
@property (weak,nonatomic) NSString *wallid;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *headerLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;
@property (weak, nonatomic) IBOutlet UILabel *updatestatusLbl;
@property (weak, nonatomic) IBOutlet UILabel *descLbl;
@property (weak, nonatomic) IBOutlet UIView *asScroll;
@property (weak, nonatomic) IBOutlet UIView *postView;
@property (weak, nonatomic) IBOutlet UIButton *likeBtn;
@property (weak, nonatomic) IBOutlet UIButton *commentBtn;
@property (weak, nonatomic) IBOutlet UILabel *likeLbl;

@property(strong,nonatomic)NSMutableArray< postImagesfeed> *imagesName;
@property(strong,nonatomic) NSMutableArray *imagesArray2;
@property (weak, nonatomic) IBOutlet UILabel *commentLbl;


@property (nonatomic, weak) id <DashboardTableViewCellDelegate> delegate; //define MyClassDelegate as delegate
@end
