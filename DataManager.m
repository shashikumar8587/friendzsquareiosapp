//
//  DataManager.m
//  TheGovSummit
//
//  Created by Rameshwar Gupta on 07/01/15.
//  Copyright (c) 2015 PSS. All rights reserved.
//

#import "DataManager.h"
#import "userDetail.h"
static DataManager* gDataMgr = nil;

@implementation DataManager

+ (DataManager*) sharedObject
{
    if (!gDataMgr)
    {
        gDataMgr = [[DataManager alloc] init];
    }
    return gDataMgr;
}

#pragma mark -- Code to create the object of an object class

-(userDetail*) userObject{
    
    if (_user.u_id==nil) {
        _user=nil;
    }
    if (_user==Nil) {
        _user=[[userDetail alloc]init];
    }

    return  _user;
}



@end
