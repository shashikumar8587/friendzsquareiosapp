//
//  LoginViewController.h
//  friendsquare
//
//  Created by magnon on 23/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "userDetail.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <CometChatSDK/CometChat.h>
#import <Google/SignIn.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,GIDSignInUIDelegate>{
   
}
@property (weak, nonatomic) IBOutlet UITextField *userName;
- (IBAction)socialConnect:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
- (IBAction)loginMethod:(id)sender;
- (IBAction)signUp:(id)sender;
- (IBAction)forgotPwd:(id)sender;

@property (strong,nonatomic)FBSDKProfile *profile;
@property (readonly, copy, nonatomic) NSString *appID;
@end
