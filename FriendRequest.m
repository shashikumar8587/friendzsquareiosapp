//
//  FriendRequest.m
//  friendsquare
//
//  Created by magnon on 06/01/16.
//  Copyright © 2016 magnon. All rights reserved.
//

#import "FriendRequest.h"
#import "UINavigationBar+navigationBar.h"
#import "UIImageView+WebCache.h"
#import "FRequestCell.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "Status.h"
#import "Utils.h"

@implementation FriendRequest{
    NSArray *FRequestArray;
    UIImageView *noImageView;
}

-(void)viewDidLoad{
    
self.navigationItem.title = @"Friends Requests";
    [self.navigationController.navigationBar setBottomBorderColor:[UIColor colorWithRed:74/255.0 green:180/255.0 blue:85.0/255.0 alpha:1] height:1];
    [super viewDidLoad];
    noImageView.hidden = YES;
   
}
-(void)viewWillAppear:(BOOL)animated{
     [self getFRequestList];
}
-(void)btnConfirmClicked:(id)sender{
    NSLog(@"%ld",(long)[sender tag]);
    FriendsModelKeys *post = _friendsList.items[[sender tag]];
    NSLog(@"Friendlist model array is %@",post);
    
    [self respondToFRequest:@"ACCEPT" :post.u_id];
}
-(void)btnRemoveClicked:(id)sender{
    NSLog(@"%ld",(long)[sender tag]);
    FriendsModelKeys *post = _friendsList.items[[sender tag]];
    NSLog(@"Friendlist model array is %@",post);
    [self respondToFRequest:@"REJECT" :post.u_id];

}

-(void)respondToFRequest:(NSString*)actionType :(NSString *)friendId{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:GET_USERID  forKey:@"user_id"];
    [postDict setValue:friendId  forKey:@"profile_id"];
    [postDict setValue:actionType  forKey:@"status_name"];
    
    [manager POST:kBaseUrlChangeConnectionStatus parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        if([status.error isEqualToString:@"True"]){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        else{
            _friendsList=[[FriendsModel alloc]initWithDictionary:responseObject error:&err];
            [self.tableView reloadData];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }
        
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
              [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
}
-(void)getFRequestList {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    NSLog(@"FSUserid is %@",GET_USERID);
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:GET_USERID  forKey:@"user_id"];
    
    [manager POST:kBaseUrlFriendRequest parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        if([status.error isEqualToString:@"True"]){
            [Utils showAlertwithMessage:[responseObject objectForKey:@"message"]];
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        else{
            _friendsList=[[FriendsModel alloc]initWithDictionary:responseObject error:&err];
            [self.tableView reloadData];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }
        
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
              [MBProgressHUD hideHUDForView:self.view animated:YES];
              
}];
    
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_friendsList.items.count > 0){
        return _friendsList.items.count;
    }
    else {
        return 1;
    }
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    FriendsModelKeys *post = _friendsList.items[indexPath.row];
    NSLog(@"Friendlist model array is %@",post);
    
    if (_friendsList.items.count > 0) {
        static NSString *cellIdentifier = @"FRequestCell";
        
        FRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (cell == nil) {
            cell = [[FRequestCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        else {
            
        }
        NSURL *url = [NSURL URLWithString:post.u_profile_photo];
        cell.imgProfile.layer.cornerRadius = cell.imageView.frame.size.width/2;
        cell.imgProfile.clipsToBounds = YES;
        cell.imgProfile.contentMode = UIViewContentModeScaleAspectFit;
        cell.imgProfile.backgroundColor = [UIColor whiteColor];
        [cell.imgProfile sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"noimage"]];
        
        cell.btnConfirm.tag = indexPath.row;
        cell.btnRemove.tag = indexPath.row;
        
        [cell.btnConfirm addTarget:self action:@selector(btnConfirmClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnRemove addTarget:self action:@selector(btnRemoveClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.lblLocation.text = post.connection_status_name;
        cell.lblName.text     = post.u_display_name;
        return cell;
    }
    else {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        
        noImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        noImageView.image = [UIImage imageNamed:@"No_Image"];
        noImageView.center = self.view.center;
        noImageView.contentMode = UIViewContentModeScaleAspectFit;
        if (_friendsList.items.count>0) {
        [cell.contentView addSubview:noImageView];
        }
    
        
        return cell;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_friendsList.items.count > 0){
        return 80;
    }
    else {
        return self.view.frame.size.height;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    Yourstring=[catagorry objectAtIndex:indexPath.row];
//    
//    //Pushing next view
//    cntrSecondViewController *cntrinnerService = [[cntrSecondViewController alloc] initWithNibName:@"cntrSecondViewController" bundle:nil];
//    [self.navigationController pushViewController:cntrinnerService animated:YES];
    
}
@end
