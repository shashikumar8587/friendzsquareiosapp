//
//  DataManager.h
//  TheGovSummit
//
//  Created by Rameshwar Gupta on 07/01/15.
//  Copyright (c) 2015 PSS. All rights reserved.
//
#import <Foundation/Foundation.h>

@class userDetail;

@interface DataManager : NSObject{
    
    userDetail *_user;
}

+ (DataManager*) sharedObject;

// code to create a singleton object for HCUser
- (userDetail*) userObject;

@end

#define _gAppData [DataManager sharedObject]
