//
//  ProfileViewController.m
//  friendsquare
//
//  Created by magnon on 26/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "ProfileViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "LinkedInHelper.h"
#import <FBSDKAccessToken.h>
#import <Google/SignIn.h>
#import "ViewController.h"
#import "AppDelegate.h"

#import "UINavigationBar+navigationBar.h"


#import "AFNetworking.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "Status.h"
#import "Utils.h"
#import "Base64.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "NSDate+Additions.h"


@interface ProfileViewController ()<UINavigationControllerDelegate>{
    
    BOOL sidebarMenuOpen;
    
}
@end

@implementation ProfileViewController
//- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
//{
//    return YES;
//}
//
//- (BOOL)slideNavigationControllerShouldDisplayRightMenu
//{
//    return YES;
//}
- (void)viewDidLoad {
    _picView .layer.cornerRadius = _picView.frame.size.width/2;
    _picView.layer.borderColor = [UIColor whiteColor].CGColor;
    _picView.layer.borderWidth = 2.0f;
    _picView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    _picView.layer.shouldRasterize = YES;
    _picView.clipsToBounds = YES;

    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    
    NSLog(@"selected profile is %@",_selectedFriend);
    
    
    
     //  [self.navigationController setNavigationBarHidden:NO];
        
        self.navigationItem.title = @"Profile";
        [self.navigationController.navigationBar setBottomBorderColor:[UIColor colorWithRed:74/255.0 green:180/255.0 blue:85.0/255.0 alpha:1] height:1];
    //[self getDetailFromServer];
      [self setUpview];
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)getDetailFromServer{
//    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
//    
//    [postDict setValue:_selectedFriend.u_id  forKey:@"user_id"];
//    
//    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
//    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
//    
//    
//    [manager POST:kBaseUrlGetUserProfile parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"User profile is %@",responseObject);
//        NSError* err = nil;
//        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
//        
//        if([status.error isEqualToString:@"True"]){
//           
//            [MBProgressHUD hideHUDForView:self.view animated:YES];
//            //[Utils showAlertwithMessage:status.message];
//        }
//        else{
//            
//          getUserDetail = [[getUserDetailModel alloc] initWithDictionary:responseObject error:&err];
//            NSLog(@"Name is User %@",getUserDetail);
//            [self setUpview];
//            //[Utils showAlertwithMessage:postListArray];
//            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//            
//        }
//    }
//     
//     
//     
//          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//              NSLog(@"fail");
//              [MBProgressHUD hideHUDForView:self.view animated:YES];
//              
//          }];
//    
//
//}
-(void)setUpview {
    NSString *connectionStatus=_selectedFriend.connection_status;
    NSString *connectionStatusBy=_selectedFriend.connection_status_by;
    UIButton *callBtn=(UIButton *)[self.view viewWithTag:101];
    UIButton *msgBtn=(UIButton *)[self.view viewWithTag:102];
    UIButton *blockBtn=(UIButton *)[self.view viewWithTag:103];
    UIButton *addFriend=(UIButton *)[self.view viewWithTag:104];
    UIButton *accept=(UIButton *)[self.view viewWithTag:105];
    UIButton *reject=(UIButton *)[self.view viewWithTag:106];
    UIButton *remove=(UIButton *)[self.view viewWithTag:107];
     UIButton *CancelRequestBtn=(UIButton *)[self.view viewWithTag:109];
    [callBtn setHidden:YES];
    [msgBtn setHidden:YES];
    [blockBtn setHidden:YES];
    [addFriend setHidden:YES];
     [accept setHidden:YES];
     [reject setHidden:YES];
    [remove setHidden:YES];
    [CancelRequestBtn setHidden:YES];
    
    if([connectionStatus isEqualToString:@""] || [connectionStatus isEqual:[NSNull null]]){
        [addFriend setTitle:@"Add Friend" forState:UIControlStateNormal];
        [addFriend setUserInteractionEnabled:YES];
        [addFriend setHidden:NO];
        [blockBtn setHidden:YES];
    }
    else if ([connectionStatus isEqualToString:@"0"]){
        if([connectionStatusBy isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]]){
            [addFriend setHidden:NO];
            [addFriend setTitle:@"Request Sent" forState:UIControlStateNormal];
            [addFriend setUserInteractionEnabled:NO];
             [CancelRequestBtn setHidden:NO];
        }
        else{
            [accept setHidden:NO];
            [reject setHidden:NO];
             [blockBtn setHidden:YES];
        }
    }
     else if ([connectionStatus isEqualToString:@"1"]){
         [addFriend setHidden:NO];
         [addFriend setTitle:@"Connected" forState:UIControlStateNormal];
         [addFriend setUserInteractionEnabled:NO];
         [remove setHidden:NO];
         [callBtn setHidden:NO];
         [msgBtn setHidden:NO];
//         [blockBtn setImage:[UIImage imageNamed:@"Unblock"] forState:UIControlStateNormal];
//         [blockBtn setHidden:NO];
     }
    else if ([connectionStatus isEqualToString:@"3"]){
        if([connectionStatusBy isEqualToString:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]]){
            [addFriend setHidden:NO];
            
            [addFriend setTitle:@"Add Friend" forState:UIControlStateNormal];
            [addFriend setUserInteractionEnabled:YES];

            NSLog(@"unblock dikhana hai");
        }
        else{
            
        }
    }
    
    
    
    
 // getUserDetailModel *userDetailModel=getUserDetail.u_profile_photo;
    NSString *imageUrl=_selectedFriend.u_profile_photo;
    NSURL *url = [NSURL URLWithString:imageUrl];
    
    // download the image asynchronously
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self downloadImageWithURL:url completionBlock:^(BOOL succeeded, UIImage *image) {
                if (succeeded) {
                    // change the image in the cell
                    _picView.image = image;
                    
                    // cache the image for use later (when scrolling up)
                    //img.image = image;
                }
            }];
        });
    });

    
    _nameLbl.text=[Utils fullName:_selectedFriend.u_first_name :_selectedFriend.u_last_name];//[post valueForKey:@"u_first_name"];
    
    _designationLbl.text=_selectedFriend.u_current_profession;
   _addressLbl.text=[Utils completeaddress:_selectedFriend.u_state :_selectedFriend.u_country];
    _friendsLbl.text=_selectedFriend.total_connection;
}
- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (BOOL)isLinkedInAccessTokenValid {
    return [LinkedInHelper sharedInstance].isValidToken;
}
/*- (IBAction)logoutMethod:(id)sender {
    
    
//    if(sidebarMenuOpen == YES){
//        [self.view setUserInteractionEnabled:NO];
//           } else {
    [comtChat logoutWithSuccess:^(NSDictionary *response) {
    } failure:^(NSError *error) {
    }];

    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
    [navigationArray removeAllObjects];
    
    [[AppDelegate GetAppDelegate] logout];
    // [[Twitter sharedInstance ] logOut];
    if([self isLinkedInAccessTokenValid]){
        LinkedInHelper *linkedIn = [LinkedInHelper sharedInstance];
        [linkedIn logout];
    }
    if([FBSDKAccessToken currentAccessToken]!=nil){
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
        
    }
    if([[Twitter sharedInstance] session])
       {
           [[Twitter sharedInstance] logOut];
       }
    if([GIDSignIn sharedInstance]){
    [[GIDSignIn sharedInstance] signOut];
    }
    ViewController *objHome =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:objHome animated:NO];
          // }

}


//- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if(sidebarMenuOpen == YES){
//        return nil;
//    } else {
//        return indexPath;
//    }
//}
// *///{"user_id":"1","profile_id":"104","status_name":"SEND"}
//action_code possible values: "SEND","ACCEPT","REJECT","BLOCK","UNBLOCK","DISCONNECT"

- (IBAction)RequestUpdate:(id)sender {
     NSString *requestStatus;
    switch ([sender tag]) {
        case 103:
            requestStatus=@"BLOCK";
            break;
        case 104:
            requestStatus=@"SEND";
            break;
        case 105:
            requestStatus=@"ACCEPT";
            break;
        case 106:
            requestStatus=@"REJECT";
            break;
        case 107:
            requestStatus=@"DISCONNECT";
            break;
        case 109:
            requestStatus=@"CANCEL";
            break;
        
        default:
            break;
    }
   
    
    
   
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]  forKey:@"user_id"];
    [postDict setValue:_selectedFriend.u_id  forKey:@"profile_id"];
    [postDict setValue:requestStatus  forKey:@"status_name"];
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlChangeConnectionStatus parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Request Response is %@",responseObject);
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            //[Utils showAlertwithMessage:status.message];
        }
        else{
            
            
//            NSString *connectionStatus=_selectedFriend.connection_status;
//            NSString *connectionStatusBy=_selectedFriend.connection_status_by;

           
            if([requestStatus isEqualToString:@"SEND"]){
    _selectedFriend.connection_status_by=[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid];
                      _selectedFriend.connection_status=@"0";
               
          
            }
          
            else if ([requestStatus isEqualToString:@"DISCONNECT"]){
                 _selectedFriend.connection_status_by=[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid];
                _selectedFriend.connection_status=@"3";
            }
            else if([requestStatus isEqualToString:@"ACCEPT"]){
                _selectedFriend.connection_status_by=[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid];
                _selectedFriend.connection_status=@"1";
            }
            else if([requestStatus isEqualToString:@"REJECT"]){
                _selectedFriend.connection_status_by=[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid];
                _selectedFriend.connection_status=@"";
            }
            else if([requestStatus isEqualToString:@"CANCEL"]){
                _selectedFriend.connection_status_by=[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid];
                _selectedFriend.connection_status=@"";
            }



            [self setUpview];

            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }
    }
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
              [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
    
    

}
@end
