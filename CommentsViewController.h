//
//  CommentsViewController.h
//  friendsquare
//
//  Created by magnon on 02/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentList.h"
@protocol CommentPopupDelegate;
@interface CommentsViewController : UIViewController<UITextViewDelegate,UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray *commentListArray;
    CommentList *_comments;

}


- (IBAction)addComment:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *baseView;

@property (weak, nonatomic) IBOutlet UITextView *addcommentTextview;
@property (nonatomic, assign) NSInteger startNo;
@property (nonatomic, assign) NSInteger limit;
@property(strong,nonatomic)NSString *wallId;
@property (strong,nonatomic)NSMutableArray *commentListArray;

@property (assign, nonatomic) id <CommentPopupDelegate>delegate;
- (IBAction)closeTbl:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *commentTbl;
@end



@protocol CommentPopupDelegate<NSObject>
@optional
-(void)sendChangedPaswdToServer:(CommentsViewController *)aSecondDetailViewController;

- (void)cancelButtonClicked:(CommentsViewController*)changePswdViewController;
@end