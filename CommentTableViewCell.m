//
//  CommentTableViewCell.m
//  friendsquare
//
//  Created by magnon on 02/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "CommentTableViewCell.h"

@implementation CommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
    _picView.translatesAutoresizingMaskIntoConstraints = YES;
//    _headerLbl.translatesAutoresizingMaskIntoConstraints = YES;
//    
//    _dateLbl.translatesAutoresizingMaskIntoConstraints = YES;
//    _commenttextLbl.translatesAutoresizingMaskIntoConstraints = YES;
//    [_commenttextLbl sizeToFit];
    [self imageSetup];
}
-(void)imageSetup
{
   _picView .layer.cornerRadius = _picView.frame.size.width/2;
    _picView.clipsToBounds = YES;
    _picView.contentMode = UIViewContentModeScaleAspectFit;
    _picView.backgroundColor = [UIColor whiteColor];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        //        self.label.lineBreakMode = NSLineBreakByWordWrapping;
        //        self.label.numberOfLines = 0;
        //        self.label.translatesAutoresizingMaskIntoConstraints = NO;
        //
        //        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-6-[bodyLabel]-6-|" options:0 metrics:nil views:@{ @"bodyLabel": self.label }]];
        //        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-6-[bodyLabel]-6-|" options:0 metrics:nil views:@{ @"bodyLabel": self.label }]];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // Make sure the contentView does a layout pass here so that its subviews have their frames set, which we
    // need to use to set the preferredMaxLayoutWidth below.
    [self.contentView setNeedsLayout];
    [self.contentView layoutIfNeeded];
    
    // Set the preferredMaxLayoutWidth of the mutli-line bodyLabel based on the evaluated width of the label's frame,
    // as this will allow the text to wrap correctly, and as a result allow the label to take on the correct height.
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
        _commenttextLbl.preferredMaxLayoutWidth = 450;
    }
    else{
        _commenttextLbl.preferredMaxLayoutWidth = 200;
    }
  // _commenttextLbl.preferredMaxLayoutWidth=CGRectGetWidth(self.bounds);
    //CGRectGetWidth(self.contentView.frame.size.width);
}



@end
