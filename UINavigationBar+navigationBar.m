//
//  UINavigationBar+navigationBar.m
//  friendsquare
//
//  Created by magnon on 20/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "UINavigationBar+navigationBar.h"

@implementation UINavigationBar (navigationBar)
- (void)setBottomBorderColor:(UIColor *)color height:(CGFloat)height{
    CGRect bottomBorderRect = CGRectMake(0, CGRectGetHeight(self.frame), CGRectGetWidth(self.frame), height);
    UIView *bottomBorder = [[UIView alloc] initWithFrame:bottomBorderRect];
    [bottomBorder setBackgroundColor:color];
    [self addSubview:bottomBorder];
    
    
}
@end

