//
//  Connections.h
//  friendsquare
//
//  Created by magnon on 20/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FriendsModel.h"
@interface Connections : UIViewController{
    FriendsModel *_friendsList;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;



@property (weak, nonatomic) IBOutlet UITableView *connectionTableView;
;

@property (nonatomic, strong) NSMutableArray *searchResult;

@end
