//
//  UINavigationBar+navigationBar.h
//  friendsquare
//
//  Created by magnon on 20/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationBar (navigationBar)
- (void)setBottomBorderColor:(UIColor *)color height:(CGFloat)height;
@end
