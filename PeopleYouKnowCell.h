//
//  PeopleYouKnowCell.h
//  friendsquare
//
//  Created by magnon on 22/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PeopleYouKnowCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *suggestionLbl;
@property (weak, nonatomic) IBOutlet UIButton *addFriendBtn;

@end
