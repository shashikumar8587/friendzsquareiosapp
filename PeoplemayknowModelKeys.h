//
//  PeoplemayknowModelKeys.h
//  friendsquare
//
//  Created by magnon on 22/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//
//"u_city" = "";
//"u_college_specialization" = "";
//"u_country" = "";
//"u_current_profession" = "";
//"u_display_name" = "Arvind Shishodia";
//"u_education_level" = "";
//"u_first_name" = Arvind;
//"u_gender" = "";
//"u_guid" = 1450099017cec438f4a949c58e2c5fd3af1dd7f9d4;
//"u_id" = 97;
//"u_last_name" = Shishodia;
//"u_marital_status" = "";
//"u_profile_photo" = "http://magnon.co.in/uploads/profile_photos/blank_avatar.jpg";
//"u_state" = "";
#import <JSONModel/JSONModel.h>
@protocol PeoplemayknowModelKeys
@end
@interface PeoplemayknowModelKeys : JSONModel




@property (strong, nonatomic) NSString* u_city;
@property (strong, nonatomic) NSString* u_college_specialization;
@property (strong, nonatomic) NSString* u_country;
@property (strong, nonatomic) NSString* u_current_profession;
@property (strong, nonatomic) NSString* u_display_name;
@property (strong, nonatomic) NSString* u_education_level;
//@property (strong, nonatomic) NSString* u_email;
@property (strong, nonatomic) NSString* u_first_name;
@property (strong, nonatomic) NSString* u_gender;
@property (strong, nonatomic) NSString* u_guid;

@property (strong, nonatomic) NSString* u_id;
@property (strong, nonatomic) NSString* u_last_name;
@property (strong, nonatomic) NSString* u_marital_status;
//@property (strong, nonatomic) NSString* u_phone_number;
@property (strong, nonatomic) NSString* u_profile_photo;
//@property (strong, nonatomic) NSString* u_state;

//@property (strong, nonatomic) NSString* u_zip_code;


@end
