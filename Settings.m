//
//  Settings.m
//  friendsquare
//
//  Created by magnon on 10/01/16.
//  Copyright © 2016 magnon. All rights reserved.
//

#import "Settings.h"
#import <TwitterKit/TwitterKit.h>
#import "LinkedInHelper.h"
#import <FBSDKAccessToken.h>
#import <Google/SignIn.h>

#import "HomeViewController.h"
#import "Connections.h"
#import "Constants.h"

#import "userDetail.h"
#import "AppDelegate.h"
#import "ProfileViewController.h"

#import "ViewController.h"
#import "Utils.h"
#import "MyProfileViewController.h"

@implementation Settings

-(void)viewDidLoad{
   
    cometChat=[[CometChat alloc]initWithAPIKey:@""];
    [super viewDidLoad];
}
- (IBAction)logoutMethod:(id)sender {
    [cometChat logoutWithSuccess:^(NSDictionary *response) {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGIN_DETAILS];
        //[self.navigationController popViewControllerAnimated:YES];
        [PushNotification unsubscribeAllParseChannels];
        
    } failure:^(NSError *error) {
        
    }];
    NSMutableArray *navigationArray = [[NSMutableArray alloc] initWithArray:self.navigationController.viewControllers];
    [navigationArray removeAllObjects];
    
    [[AppDelegate GetAppDelegate] logout];
    // [[Twitter sharedInstance ] logOut];
    if([self isLinkedInAccessTokenValid]){
        LinkedInHelper *linkedIn = [LinkedInHelper sharedInstance];
        [linkedIn logout];
    }
    if([FBSDKAccessToken currentAccessToken]!=nil){
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];
        
    }
    if([[Twitter sharedInstance] session])
    {
        [[Twitter sharedInstance] logOut];
    }
    if([GIDSignIn sharedInstance]){
        [[GIDSignIn sharedInstance] signOut];
    }
    ViewController *homeVC=[MainStoryBoard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:homeVC animated:YES];
   
    



}
- (BOOL)isLinkedInAccessTokenValid {
    return [LinkedInHelper sharedInstance].isValidToken;
}


@end
