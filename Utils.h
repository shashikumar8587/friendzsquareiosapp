//
//  Utils.h
//  friendsquare
//
//  Created by magnon on 26/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"
#import <UIKit/UIKit.h>

@interface Utils : NSObject
{
    
}
+(void )showAlertwithMessage:(NSString *) msg;

+(NSString *)fullName :(NSString *)firstName :(NSString *)lastName;
+(NSArray *)imagesArray:(NSString *)str;
+(NSString *)completeaddress :(NSString *)state :(NSString *)country;
+ (UIImage *)compressImage:(UIImage *)image;
@end
