//
//  LoginViewController.m
//  friendsquare
//
//  Created by magnon on 23/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "LoginViewController.h"
#import "userDetail.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "Status.h"
#import "Utils.h"
#import "RegisterViewController.h"
#import <TwitterKit/TwitterKit.h>
#import "LinkedInHelper.h"
#import <FBSDKAccessToken.h>
#import "HomeViewController.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>

#import "NativeKeys.h"
#import "DataManager.h"

@interface LoginViewController (){
    
    NSString *twitterName;
    NSString *twitterEmail;
    NSString *twitterid;
    CometChat *cometChat;
    
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpacing;
@end

@implementation LoginViewController
- (void)viewDidLoad {
    
    
  cometChat = [[CometChat alloc] initWithAPIKey:@""];
    
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    if (GET_USERID) {
        [[AppDelegate GetAppDelegate] tabShow];
        HomeViewController *objHome =[self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        [self.navigationController pushViewController:objHome animated:NO];
        objHome=nil;
    }
    
    [CometChat setDevelopmentMode:YES];
    
     NSLog(@"SDK Version is %@",[CometChat getSDKVersion]);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(connectWithGoogle)
                                                 name:@"connectWithGoogle"
                                               object:nil];
     [self.navigationController setNavigationBarHidden:NO];
     [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
//    _userName.text=@"shashikumar.8587@gmail.com";
//    _passwordTxt.text=@"123";
    [self.navigationController setNavigationBarHidden:YES];
    [super viewWillAppear:YES];
}
- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"%lu",(unsigned long)twitterEmail.length);
    
    if (twitterEmail.length==0) {
        if ([[Twitter sharedInstance] session]) {
            TWTRShareEmailViewController *shareEmailViewController = [[TWTRShareEmailViewController alloc] initWithCompletion:^(NSString *email, NSError* error) {
                NSLog(@"Email %@, Error: %@", email, error);
                twitterEmail=email;
                [self connectWithTwitter:twitterEmail :twitterName :twitterid];
            }];
            [self presentViewController:shareEmailViewController animated:YES completion:^{
                
            }];
        }
    }
}







- (void) dealloc
{
    // If you don't remove yourself as an observer, the Notification Center
    // will continue to try and send notification objects to the deallocated
    // object.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    //[super dealloc];
}

- (void)fetchUserInformations {
//    if([self isLinkedInAccessTokenValid]){
//        HomeViewController *home=[MainStoryBoard instantiateViewControllerWithIdentifier:@"Home"];
//        [self.navigationController pushViewController:home animated:YES];
//    }
//    else{
    LinkedInHelper *linkedIn = [LinkedInHelper sharedInstance];
    
    linkedIn.cancelButtonText = @"Close"; // Or any other language But Default is Close
    
    NSArray *permissions = @[@(BasicProfile),
                             @(EmailAddress),
                             @(Share),
                             @(CompanyAdmin)];
    
    linkedIn.showActivityIndicator = YES;
    
    
    [linkedIn requestMeWithSenderViewController:self
                                       clientId:@"75ri408sb048pt"         // Your App Client Id
                                   clientSecret:@"ffGeWEBlbDweVyvc"         // Your App Client Secret
                                    redirectUrl:@"https://www.example.com"         // Your App Redirect Url
                                    permissions:permissions
                                          state:@""               // Your client state
                                successUserInfo:^(NSDictionary *userInfo) {
                                    // Whole User Info
                                    NSLog(@"user Info : %@", userInfo);
                                    NSString *linkedInemail=[userInfo objectForKey:@"emailAddress"];
                                    NSLog(@"user email : %@", linkedInemail);
                                    NSString *linkedinfirstName=[userInfo objectForKey:@"firstName"];
                                    NSString *linkedInLastname =[userInfo objectForKey:@"lastName"];
                                    
                                    NSString *linkedInID=[userInfo objectForKey:@"id"];
                                    
                                    
                                    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
                                    [dict setObject:linkedInID forKey:@"social_id"];
                                    [dict setObject:linkedinfirstName forKey:@"firstname"];
                                    [dict setObject:linkedInLastname forKey:@"lastname"];
                                    [dict setObject:linkedInemail forKey:@"email"];
                                    [dict setObject:@"" forKey:@"phone"];
                                    [dict setObject:@"LinkedIn" forKey:@"type"];
                                    
                                    [self connectWithServer:dict];

                                    
                                    
                                }
                              failUserInfoBlock:^(NSError *error) {
                                  NSLog(@"error : %@", error.userInfo.description);
                              }
     ];
    
}
- (BOOL)isLinkedInAccessTokenValid {
    return [LinkedInHelper sharedInstance].isValidToken;
}
-  (void)getUserInfo {
    
    LinkedInHelper *linkedIn = [LinkedInHelper sharedInstance];
    
    // If user has already connected via linkedin in and access token is still valid then
    // No need to fetch authorizationCode and then accessToken again!
    
#warning - To fetch user info  automatically without getting authorization code, accessToken must be still valid
    
    if (linkedIn.isValidToken) {
        
        // So Fetch member info by elderyly access token
        [linkedIn autoFetchUserInfoWithSuccess:^(NSDictionary *userInfo) {
            // Whole User Info
            NSLog(@"user Info : %@", userInfo);
        } failUserInfo:^(NSError *error) {
            NSLog(@"error : %@", error.userInfo.description);
        }];
    }
}
- (IBAction)socialConnect:(id)sender {
    NSLog(@"social network button");
    
    if([sender tag]==101){
        NSLog(@"Facebook button is clicked");
        [AppDelegate GetAppDelegate].isFacebook=YES;
       
    if([FBSDKAccessToken currentAccessToken]!=nil)
    {
        [FBSDKProfile setCurrentProfile:_profile];
        
        
       // NSLog(@"User name: %@",[FBSDKProfile currentProfile].name);
        
        [self getDetailsAndLogin];
    }
    else{
        
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    
       [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         NSLog(@"result is %@",result);
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             if ([result.grantedPermissions containsObject:@"email"]) {
                 // Do work
                 [self getDetailsAndLogin];
                 
             }
              NSLog(@"Logged in%@",result.token);
              NSLog(@"User name: %@",[FBSDKProfile currentProfile].name);
         }
          }];
    }
    }
    else if ([sender tag]==102){
        
        NSLog(@"twitter button is clicked");
        //[Utils showAlertwithMessage:@"In Progress"];
        
        // Objective-C
        if ([[Twitter sharedInstance] session]) {
            TWTRShareEmailViewController* shareEmailViewController = [[TWTRShareEmailViewController alloc] initWithCompletion:^(NSString* email, NSError* error) {
                NSLog(@"Email %@, Error: %@", email, error);
                twitterEmail=email;
            }];
            [self presentViewController:shareEmailViewController animated:YES completion:nil];
        } else {
            // TODO: Handle user not signed in (e.g. attempt to log in or show an alert)
            [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
                if (session) {
                    
                    twitterid=[session userID];
                    twitterName=[session userName];
                    
                    //[self connectWithTwitter:twitterEmail :twitterName :twitterid];
                    //NSLog(@"twitter display email%@",[session email]);
                } else {
                    NSLog(@"error: %@", [error localizedDescription]);
                }
            }];
        }
        
      
        
    }
    else if ([sender tag]== 103){
        NSLog(@"Google button is clicked");
        [[GIDSignIn sharedInstance] signIn];
    }
    else{
        NSLog(@"LinkedIn button is clicked");
        [self fetchUserInformations];
    }
}
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
#pragma Twitter Connection
//-(void)connectWithTwitter :(NSString *)name :(NSString *)twitterID{
-(void)connectWithTwitter :(NSString *)email :(NSString *)name :(NSString *)twitterID{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
        NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
    [userInfo setValue:name forKey:@"firstname"];
    [userInfo setValue:@"" forKey:@"lastname"];
    [userInfo setValue:email forKey:@"email"];
    [userInfo setValue:@"" forKey:@"phone"];
    [userInfo setValue:twitterID forKey:@"social_id"];
    [userInfo setValue:@"Twitter" forKey:@"type"];
    
    
   AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlSocial parameters:userInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        // [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Utils showAlertwithMessage:status.message];
        }
        else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            userDetail* user_detail = [[userDetail alloc] initWithDictionary:responseObject error:&err];
            NSLog(@"Name is User %@",user_detail.u_id);
            if(user_detail.u_id!=nil){
                
                
                //dont remove this
                NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                //[defaults setObject:user_detail.userid forKey:GET_USERID];
                [defaults setObject:user_detail.total_connection forKey:FSTotalConnection];
                [defaults setObject:user_detail.u_id forKey:FSUserid];
                [defaults setObject:user_detail.u_first_name forKey:FSfirstname];
                [defaults setObject:user_detail.u_last_name   forKey:FSlastname];
                [defaults setObject:user_detail.u_email forKey:FSemail];
                [defaults setObject:user_detail.u_display_name forKey:FSDisplayname];
                [defaults setObject:user_detail.u_gender forKey:FSgender];
                [defaults setObject:user_detail.u_personal_visibility forKey:FSpersonalVisibility];
                [defaults setObject:user_detail.u_marital_status forKey:FSmaritalstatus];
                
                [defaults setObject:user_detail.u_profile_photo forKey:FSprofilePhoto];
                [defaults setObject:user_detail.u_country forKey:FScountry];
                [defaults setObject:user_detail.u_state forKey:FSState];
                [defaults setObject:user_detail.u_city forKey:FSCity];
                [defaults setObject:user_detail.u_zip_code forKey:FSZipCode];
                [defaults setObject:user_detail.u_phone_number forKey:FSPhoneNumber];
                [defaults setObject:user_detail.u_qualification_visibility forKey:FSQualificationVisibility];
                [defaults setObject:user_detail.u_education_level forKey:FSEducationLevel];
                [defaults setObject:user_detail.u_college_specialization forKey:FSCollegeSpecialization];
                [defaults setObject:user_detail.u_graduate_year forKey:FSGraduateYear];
                [defaults setObject:user_detail.u_study_status forKey:FSStudyStatus];
                [defaults setObject:user_detail.u_education_establishment forKey:FSEducationEstablishment];
                [defaults setObject:user_detail.u_country_establishment forKey:FSCountryEstablishment];
                [defaults setObject:user_detail.u_city_establishment forKey:FSCityEstablishment];
                [defaults setObject:user_detail.u_professional_visibility forKey:FSProfessionalVisibility];
                [defaults setObject:user_detail.u_current_profession forKey:FSCurrentProfession];
                [defaults setObject:user_detail.u_sub_profession forKey:FSSubProfession];
                [defaults setObject:user_detail.u_current_position forKey:FSCurrentPosition];
                [defaults setObject:user_detail.u_doc forKey:FSUDoc];
                [defaults synchronize];
                [self logininCometchat:user_detail.u_id];
                
            }
            else{
                [Utils showAlertwithMessage:@"Error in response!"];
            }
            
            
            
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"fail");
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];
   
    

}
#pragma GooglePlus Connection
-(void)connectWithGoogle{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
    
    NSString *name=[[NSUserDefaults standardUserDefaults]valueForKey:@"FULLNAME"];
    NSString *socialID=[[NSUserDefaults standardUserDefaults]valueForKey:@"social_id"];
    NSString *email=[[NSUserDefaults standardUserDefaults]valueForKey:@"EMAIL"];
     NSLog(@">>>>name%@>>>>socialid%@>>>>>>%@",name,socialID,email);
    
    [userInfo setValue:name forKey:@"firstname"];
    [userInfo setValue:@"" forKey:@"lastname"];
    [userInfo setValue:email forKey:@"email"];
    [userInfo setValue:@"" forKey:@"phone"];
    [userInfo setValue:socialID forKey:@"social_id"];
    [userInfo setValue:@"GooglePlus" forKey:@"type"];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlSocial parameters:userInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        // [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Utils showAlertwithMessage:status.message];
        }
        else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            userDetail* user_detail = [[userDetail alloc] initWithDictionary:responseObject error:&err];
            NSLog(@"Name is User %@",user_detail.u_id);
            if(user_detail.u_id!=nil){
                
                
                //dont remove this
                NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                //[defaults setObject:user_detail.userid forKey:GET_USERID];
                [defaults setObject:user_detail.total_connection forKey:FSTotalConnection];
                [defaults setObject:user_detail.u_id forKey:FSUserid];
                [defaults setObject:user_detail.u_first_name forKey:FSfirstname];
                [defaults setObject:user_detail.u_last_name   forKey:FSlastname];
                [defaults setObject:user_detail.u_email forKey:FSemail];
                [defaults setObject:user_detail.u_display_name forKey:FSDisplayname];
                [defaults setObject:user_detail.u_gender forKey:FSgender];
                [defaults setObject:user_detail.u_personal_visibility forKey:FSpersonalVisibility];
                [defaults setObject:user_detail.u_marital_status forKey:FSmaritalstatus];
                
                [defaults setObject:user_detail.u_profile_photo forKey:FSprofilePhoto];
                [defaults setObject:user_detail.u_country forKey:FScountry];
                [defaults setObject:user_detail.u_state forKey:FSState];
                [defaults setObject:user_detail.u_city forKey:FSCity];
                [defaults setObject:user_detail.u_zip_code forKey:FSZipCode];
                [defaults setObject:user_detail.u_phone_number forKey:FSPhoneNumber];
                [defaults setObject:user_detail.u_qualification_visibility forKey:FSQualificationVisibility];
                [defaults setObject:user_detail.u_education_level forKey:FSEducationLevel];
                [defaults setObject:user_detail.u_college_specialization forKey:FSCollegeSpecialization];
                [defaults setObject:user_detail.u_graduate_year forKey:FSGraduateYear];
                [defaults setObject:user_detail.u_study_status forKey:FSStudyStatus];
                [defaults setObject:user_detail.u_education_establishment forKey:FSEducationEstablishment];
                [defaults setObject:user_detail.u_country_establishment forKey:FSCountryEstablishment];
                [defaults setObject:user_detail.u_city_establishment forKey:FSCityEstablishment];
                [defaults setObject:user_detail.u_professional_visibility forKey:FSProfessionalVisibility];
                [defaults setObject:user_detail.u_current_profession forKey:FSCurrentProfession];
                [defaults setObject:user_detail.u_sub_profession forKey:FSSubProfession];
                [defaults setObject:user_detail.u_current_position forKey:FSCurrentPosition];
                [defaults setObject:user_detail.u_doc forKey:FSUDoc];
                [defaults synchronize];
                [self logininCometchat:user_detail.u_id];

                
            }
            else{
                [Utils showAlertwithMessage:@"Error in response!"];
            }
            
            
            
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"fail");
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];
    
    
    
}
#pragma login with Facebook and Linkedin
-(void)connectWithServer:(NSMutableDictionary *)dict{
    
    //{"firstname":"sunil","lastname":"kumar","email":"rampal1212@gmail.com","phone":"","social_id":"25555555","type":"facebook"}
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
    [userInfo setValue:[dict objectForKey:@"firstname"] forKey:@"firstname"];
    [userInfo setValue:[dict objectForKey:@"lastname"] forKey:@"lastname"];
    [userInfo setValue:[dict objectForKey:@"email"] forKey:@"email"];
    [userInfo setValue:[dict objectForKey:@"phone"] forKey:@"phone"];
    [userInfo setValue:[dict objectForKey:@"social_id"] forKey:@"social_id"];
    [userInfo setValue:[dict objectForKey:@"type"] forKey:@"type"];
        
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrlSocial parameters:userInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        // [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Utils showAlertwithMessage:status.message];
        }
        else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            userDetail* user_detail = [[userDetail alloc] initWithDictionary:responseObject error:&err];
            NSLog(@"Name is User %@",user_detail.u_id);
            if(user_detail.u_id!=nil){
                
                
                //dont remove this
                NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                //[defaults setObject:user_detail.userid forKey:GET_USERID];
                [defaults setObject:user_detail.total_connection forKey:FSTotalConnection];
                [defaults setObject:user_detail.u_id forKey:FSUserid];
                [defaults setObject:user_detail.u_first_name forKey:FSfirstname];
                [defaults setObject:user_detail.u_last_name   forKey:FSlastname];
                [defaults setObject:user_detail.u_email forKey:FSemail];
                [defaults setObject:user_detail.u_display_name forKey:FSDisplayname];
                [defaults setObject:user_detail.u_gender forKey:FSgender];
                [defaults setObject:user_detail.u_personal_visibility forKey:FSpersonalVisibility];
                [defaults setObject:user_detail.u_marital_status forKey:FSmaritalstatus];
                
                [defaults setObject:user_detail.u_profile_photo forKey:FSprofilePhoto];
                [defaults setObject:user_detail.u_country forKey:FScountry];
                [defaults setObject:user_detail.u_state forKey:FSState];
                [defaults setObject:user_detail.u_city forKey:FSCity];
                [defaults setObject:user_detail.u_zip_code forKey:FSZipCode];
                [defaults setObject:user_detail.u_phone_number forKey:FSPhoneNumber];
                [defaults setObject:user_detail.u_qualification_visibility forKey:FSQualificationVisibility];
                [defaults setObject:user_detail.u_education_level forKey:FSEducationLevel];
                [defaults setObject:user_detail.u_college_specialization forKey:FSCollegeSpecialization];
                [defaults setObject:user_detail.u_graduate_year forKey:FSGraduateYear];
                [defaults setObject:user_detail.u_study_status forKey:FSStudyStatus];
                [defaults setObject:user_detail.u_education_establishment forKey:FSEducationEstablishment];
                [defaults setObject:user_detail.u_country_establishment forKey:FSCountryEstablishment];
                [defaults setObject:user_detail.u_city_establishment forKey:FSCityEstablishment];
                [defaults setObject:user_detail.u_professional_visibility forKey:FSProfessionalVisibility];
                [defaults setObject:user_detail.u_current_profession forKey:FSCurrentProfession];
                [defaults setObject:user_detail.u_sub_profession forKey:FSSubProfession];
                [defaults setObject:user_detail.u_current_position forKey:FSCurrentPosition];
                [defaults setObject:user_detail.u_doc forKey:FSUDoc];
                [defaults synchronize];
                [self logininCometchat:user_detail.u_id];
                
            }
            else{
                [Utils showAlertwithMessage:@"Error in response!"];
            }
            
            
            
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"fail");
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];

    
    
   }

-(void)getDetailsAndLogin{
     //{"firstname":"sunil","lastname":"kumar","email":"rampal1212@gmail.com","phone":"","social_id":"25555555","type":"facebook"}
    if([FBSDKAccessToken currentAccessToken]!=nil)
    {
        [FBSDKProfile setCurrentProfile:_profile];

    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me/?fields=first_name,last_name,name,email" parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             NSString *userID = [[FBSDKAccessToken currentAccessToken] userID];
             //NSString *userName = [result valueForKey:@"name"];
             NSString *firstName = [result valueForKey:@"first_name"];
             NSString *lastName = [result valueForKey:@"last_name"];
             
             NSString *email =  [result valueForKey:@"email"];
//             NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [[FBSDKAccessToken currentAccessToken] userID]];
              NSLog(@"signed in as %@ >>>>>>>>%@>>>>>%@>>>>%@", userID,firstName,lastName,email);
             
             NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
             [dict setObject:userID forKey:@"social_id"];
             [dict setObject:firstName forKey:@"firstname"];
             [dict setObject:lastName forKey:@"lastname"];
             [dict setObject:email forKey:@"email"];
             [dict setObject:@"123" forKey:@"phone"];
             [dict setObject:@"Facebook" forKey:@"type"];
             
             [self connectWithServer:dict];
             
             
             
                   }
         else{
            
         }
     }];
    }
}



- (IBAction)loginMethod:(id)sender {
   if ([self checkValidation]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
    [userInfo setValue:[_userName.text lowercaseString] forKey:@"u_email"];
    [userInfo setValue:_passwordTxt.text forKey:@"u_password"];
//       [cometChat loginWithURL:kBaseUrl username:_userName.text password:_passwordTxt.text success:^(NSDictionary *response) {
//       
//           NSLog(@"SDK log : Username/Password Login Success %@",response);
//       
//           //[self handleLogin];
//       
//       
//       } failure:^(NSError *error) {
//           NSLog(@"SDK log : Username/Password Login Error%@",error);
//           //[self handleLoginError:@[@1,error]];
//           
//       }];
//
       
       
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaseUrl parameters:userInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
       // [MBProgressHUD hideHUDForView:self.view animated:YES];
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
             [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Utils showAlertwithMessage:status.message];
        }
        else{
             [MBProgressHUD hideHUDForView:self.view animated:YES];
            userDetail *user_detail=[_gAppData userObject];
            [user_detail destroyPreference];

            user_detail = [[userDetail alloc] initWithDictionary:responseObject error:&err];
             NSLog(@"Name is User %@",user_detail.u_id);
            if(user_detail.u_id!=nil){
                
                
                //dont remove this
                NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
                //[defaults setObject:user_detail.userid forKey:GET_USERID];
                [defaults setObject:user_detail.total_connection forKey:FSTotalConnection];
                [defaults setObject:user_detail.u_id forKey:FSUserid];
                [defaults setObject:user_detail.u_first_name forKey:FSfirstname];
                [defaults setObject:user_detail.u_last_name   forKey:FSlastname];
                [defaults setObject:user_detail.u_email forKey:FSemail];
                [defaults setObject:user_detail.u_display_name forKey:FSDisplayname];
                [defaults setObject:user_detail.u_gender forKey:FSgender];
                [defaults setObject:user_detail.u_personal_visibility forKey:FSpersonalVisibility];
                [defaults setObject:user_detail.u_marital_status forKey:FSmaritalstatus];
                
                [defaults setObject:user_detail.u_profile_photo forKey:FSprofilePhoto];
                [defaults setObject:user_detail.u_country forKey:FScountry];
                [defaults setObject:user_detail.u_state forKey:FSState];
                [defaults setObject:user_detail.u_city forKey:FSCity];
                [defaults setObject:user_detail.u_zip_code forKey:FSZipCode];
                [defaults setObject:user_detail.u_phone_number forKey:FSPhoneNumber];
                [defaults setObject:user_detail.u_qualification_visibility forKey:FSQualificationVisibility];
                [defaults setObject:user_detail.u_education_level forKey:FSEducationLevel];
                [defaults setObject:user_detail.u_college_specialization forKey:FSCollegeSpecialization];
                [defaults setObject:user_detail.u_graduate_year forKey:FSGraduateYear];
                [defaults setObject:user_detail.u_study_status forKey:FSStudyStatus];
                [defaults setObject:user_detail.u_education_establishment forKey:FSEducationEstablishment];
                [defaults setObject:user_detail.u_country_establishment forKey:FSCountryEstablishment];
                [defaults setObject:user_detail.u_city_establishment forKey:FSCityEstablishment];
                [defaults setObject:user_detail.u_professional_visibility forKey:FSProfessionalVisibility];
                [defaults setObject:user_detail.u_current_profession forKey:FSCurrentProfession];
                [defaults setObject:user_detail.u_sub_profession forKey:FSSubProfession];
                [defaults setObject:user_detail.u_current_position forKey:FSCurrentPosition];
                [defaults setObject:user_detail.u_doc forKey:FSUDoc];
                [defaults synchronize];
                [self logininCometchat:user_detail.u_id];
//                [cometChat loginWithURL:KBaseUrlCommetchat userID:[user_detail.u_id] success:^(NSDictionary *response) {
//                    
//                    [[NSUserDefaults standardUserDefaults] setObject:@[LOGIN_TYPE_USERNAME,_userName.text,_passwordTxt.text] forKey:LOGIN_DETAILS];
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"com.friendzsquare.updateview" object:nil];
//                    HomeViewController *home=[MainStoryBoard instantiateViewControllerWithIdentifier:@"Home"];
//                    [self.navigationController pushViewController:home animated:YES];
//                    
//                } failure:^(NSError *error) {
//                    NSLog(@"comet chat login fail");
//                }];
                
               
                
                

                
                
            }
            else{
                [Utils showAlertwithMessage:@"Error in response!"];
            }

           

            
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"fail");
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }];
   }
    
}
-(void)logininCometchat:(NSString *)userid{
    [cometChat loginWithURL:KBaseUrlCommetchat userID:userid success:^(NSDictionary *response) {
        
        [[NSUserDefaults standardUserDefaults] setObject:@[LOGIN_TYPE_USERNAME,_userName.text,_passwordTxt.text] forKey:LOGIN_DETAILS];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"com.friendzsquare.updateview" object:nil];
        [[AppDelegate GetAppDelegate] tabShow];
       
       
        
        HomeViewController *home=[MainStoryBoard instantiateViewControllerWithIdentifier:@"Home"];
        [self.navigationController pushViewController:home animated:YES];
        home=nil;
        
    } failure:^(NSError *error) {
        NSLog(@"comet chat login fail");
    }];

}
- (IBAction)signUp:(id)sender {
    
    RegisterViewController *registerVc=(RegisterViewController *)[MainStoryBoard instantiateViewControllerWithIdentifier:@"Register"];
    // login.user=user;
    [self.navigationController pushViewController:registerVc animated:YES];
    registerVc=nil;
}

- (IBAction)forgotPwd:(id)sender {
    
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:App_title
                                  message:@"Enter User Email."
                                  preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Email";
        
    }];
    
    
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action) {
                                                   //Do Some action here
                                                   UITextField *login = alert.textFields.firstObject;
                                                   
                                                   if([login.text isEqualToString:@""]){
                                                      [alert dismissViewControllerAnimated:YES completion:nil];
                                                       return ;
                                                   }
                                                  
                                                   BOOL isValidate=  [self validateEmail:login.text];

                                                   if (!isValidate)
                                                   {
                                                       UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter a valid email address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                                       [alert show];
                                                   }
                                                   else{
                                                       
                                                       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                                                       NSMutableDictionary *userInfo = [[NSMutableDictionary alloc]init];
                                                       [userInfo setValue:login.text forKey:@"email"];
                                                       
                                                       AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                                                       manager.requestSerializer = [AFJSONRequestSerializer serializer];
                                                       manager.responseSerializer = [AFJSONResponseSerializer serializer];
                                                       manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
                                                       
                                                       
                                                       [manager POST:kBaseUrlForgot parameters:userInfo success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                           NSLog(@"JSON: %@", responseObject);
                                                           // [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                           NSError* err = nil;
                                                           Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
                                                           
                                                           if([status.error isEqualToString:@"True"]){
                                                               [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                               [Utils showAlertwithMessage:status.message];
                                                           }
                                                           else{
                                                               [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                               [Utils showAlertwithMessage:status.message];
                                                               [alert dismissViewControllerAnimated:YES completion:nil];
                                                           }
                                                           
                                                       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                           [MBProgressHUD hideHUDForView:self.view animated:YES];
                                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                                       }];
                                                       
                                                   }
                                                   
                                               
                                                   
                                               }];
                                                   


    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    
    
    
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma textfield delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    self.verticalSpacing.constant = -90;
    
    [UIView animateWithDuration:.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    self.verticalSpacing.constant = -0;
    
    [UIView animateWithDuration:.3 animations:^{
        [self.view layoutIfNeeded];
    }];
    return YES;
}
#pragma validate textfields
- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

-(BOOL) checkValidation
{
    if (![self validateEmail:_userName.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter a valid email address." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    else if([_passwordTxt.text isEqualToString:@""])
    {
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"" message:@"Please enter your password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }
    
    return YES;
}

@end
