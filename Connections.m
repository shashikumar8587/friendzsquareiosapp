//
//  Connections.m
//  friendsquare
//
//  Created by magnon on 20/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "Connections.h"

#import "UINavigationBar+navigationBar.h"
#import "ConnectionTableViewCell.h"

#import "SearchViewController.h"
#import "AppDelegate.h"
#import "AFNetworking.h"
#import "Constants.h"
#import "MBProgressHUD.h"
#import "Status.h"
#import "Utils.h"
#import "Base64.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "NSDate+Additions.h"
#import "ProfileViewController.h"
#import <CometChatSDK/CometChat.h>
#import "ZRScrollableTabBar.h"
#import "NativeKeys.h"
@interface Connections ()<UINavigationControllerDelegate,ZRScrollableTabBarDelegate>{
   // NSArray *friendList;
    //NSArray *searchResults;
   // UISearchController *searchController1;
     CometChat *cometChat;
}
@end

@implementation Connections


- (void)viewDidLoad {
 
   
    UIButton *tmpSearch=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [tmpSearch setImage:[UIImage imageNamed:@"search.png"] forState:UIControlStateNormal];
    tmpSearch.frame=CGRectMake(0, 0, 32, 32);
    [tmpSearch addTarget:self
                  action:@selector(searchBtnclicked)
        forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *barBtnSearch=[[UIBarButtonItem alloc]initWithCustomView:tmpSearch];
    self.navigationItem.rightBarButtonItem=barBtnSearch;
   

    
    cometChat = [[CometChat alloc] initWithAPIKey:@""];
   [cometChat changeStatus:0 success:^(NSDictionary *response) {
       NSLog(@"success %@",response);
   } failure:^(NSError *error) {
       NSLog(@"status update  fail");
   }];
    
    [cometChat changeStatusMessage:@"Happy Diwali...." success:^(NSDictionary *response) {
        
        NSLog(@"success %@",response);
    } failure:^(NSError *error) {
        NSLog(@"status update  fail");
    }];
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)searchBtnclicked{
    [AppDelegate GetAppDelegate].isCancelSearch=YES;
    
    SearchViewController *search=[self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
   
    [self.navigationController pushViewController:search animated:NO];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
        [self.navigationController setNavigationBarHidden:NO];
    
   self.navigationItem.title = @"Friends";
    [self.navigationController.navigationBar setBottomBorderColor:[UIColor colorWithRed:74/255.0 green:180/255.0 blue:85.0/255.0 alpha:1] height:1];
    [self getFriendlist];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)getFriendlist{
    NSMutableDictionary *postDict = [[NSMutableDictionary alloc]init];
    
    [postDict setValue:[[NSUserDefaults standardUserDefaults]objectForKey:FSUserid]     forKey:@"user_id"];
   
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = CONTENT_TYPE_HTML;
    
    
    [manager POST:kBaserUrlgetFriendList parameters:postDict success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Friend list is %@",responseObject);
        NSError* err = nil;
        Status *status=[[Status alloc]initWithDictionary:responseObject error:&err];
        
        if([status.error isEqualToString:@"True"]){
           
             [MBProgressHUD hideHUDForView:self.view animated:YES];
            [Utils showAlertwithMessage:status.message];
            _friendsList.items=nil;
            [_connectionTableView reloadData];
        }
        else{
            
            _friendsList=[[FriendsModel alloc]initWithDictionary:responseObject error:&err];
            
            
            // self.searchFriendList=_items.items;
            
            // NSLog(@"JSON: %@", _feed.feed);
            [_connectionTableView reloadData];
            //[Utils showAlertwithMessage:postListArray];
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
        }
    }
     
     
     
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"fail");
               [Utils showAlertwithMessage:@"Oops ,Something went wrong!"];
               [MBProgressHUD hideHUDForView:self.view animated:YES];
              
          }];
    
    
}

#pragma mark-- Delegate methods of Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _friendsList.items.count;
    
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *MyIdentifier = @"ConnectionCell";
    
    ConnectionTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil)
    {
        cell = [[ConnectionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:MyIdentifier];
    }
    FriendsModelKeys *post = _friendsList.items[indexPath.row];
    
    NSLog(@"Friendlist model array is %@",post);
    
    static NSDateFormatter *dateFormatter;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    });
    
    
    // [Utils fullName:post.u_first_name :post.u_last_name];
    cell.headerLbl.text=[Utils fullName:post.u_first_name :post.u_last_name];//[post valueForKey:@"u_first_name"];
   
    
    cell.subLbl.text=[Utils completeaddress:post.u_state :post.u_country];

    
    NSURL *url = [NSURL URLWithString:post.u_profile_photo];
    cell.imgView.layer.cornerRadius = cell.imgView.frame.size.width/2;
    cell.imgView.clipsToBounds = YES;
    cell.imgView.contentMode = UIViewContentModeScaleAspectFit;
    cell.imgView.backgroundColor = [UIColor whiteColor];
    [cell.imgView sd_setImageWithURL:url
                    placeholderImage:[UIImage imageNamed:@"noimage"]];
    
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    FriendsModelKeys *post = _friendsList.items[indexPath.row];
    ProfileViewController *profile=[MainStoryBoard instantiateViewControllerWithIdentifier: @"ProfileViewController"];
    profile.selectedFriend=post;
    [self.navigationController pushViewController:profile animated:NO];

}
@end
