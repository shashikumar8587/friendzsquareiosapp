//
//  peoplemayKnowModel.h
//  friendsquare
//
//  Created by magnon on 22/12/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "PeoplemayknowModelKeys.h"

@interface peoplemayKnowModel : JSONModel
@property (strong, nonatomic) NSMutableArray<PeoplemayknowModelKeys>* items;

@end

