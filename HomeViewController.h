//
//  HomeViewController.h
//  friendsquare
//
//  Created by magnon on 03/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommentsViewController.h"
#import "postFeed.h"
#import "peoplemayKnowModel.h"
#import "postImagesfeed.h"

#import "DashboardTableViewCell.h"

@interface HomeViewController : UIViewController<UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UIScrollViewDelegate,CommentPopupDelegate,DashboardTableViewCellDelegate>{
     CommentsViewController  *commentVC;
     NSMutableArray *postImages;
    NSMutableArray *postListArray;
    postFeed *_feed;
    
    peoplemayKnowModel *_items;
}
@property (nonatomic, assign) NSInteger startNo;
@property (nonatomic, assign) NSInteger limit;
@property(strong,nonatomic)NSMutableArray *postListArray;
@property(strong,nonatomic) NSMutableArray *postImages;
@property (weak, nonatomic) IBOutlet UILabel *updatelbl;
@property (weak, nonatomic) IBOutlet UIButton *gmImagePickerButton;
@property (weak, nonatomic) IBOutlet UICollectionView *uploadCollectionView;
@property (weak, nonatomic) IBOutlet UITextView *updatePostTextview;
@property (weak, nonatomic) IBOutlet UITableView *postTbl;
@property (weak, nonatomic) IBOutlet UIView *baseView;
@property (strong, nonatomic) IBOutlet UIView *topView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIView *suggestionView;


@property (weak, nonatomic) IBOutlet UICollectionView *peopleyouknowCollectionView;
- (IBAction)addPhotoMethod:(id)sender;
- (IBAction)launchGMImagePicker:(id)sender;
- (IBAction)postUploadMethod:(id)sender;
@end
