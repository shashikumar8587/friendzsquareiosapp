//
//  DashboardTableViewCell.m
//  friendsquare
//
//  Created by magnon on 27/11/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import "DashboardTableViewCell.h"

#import "UIImageView+WebCache.h"
#import "UIImageView+AFNetworking.h"
#import "Constants.h"
#import "Utils.h"
@implementation DashboardTableViewCell
@synthesize imageView,delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        
    }
    return self;
}
-(void)awakeFromNib{
    scrollView=[[ScrollViewAdd alloc]init];
    scrollView.delegate = self;
    scrollView.scrollEnabled = YES;

   // imagesName= [[NSMutableArray alloc] init];
  

}
-(void)layoutSubviews
{
   // NSLog(@"image feed is %@",_imagesName);
    
    
    if (_imagesName == nil || [_imagesName count] == 0 || [_imagesName containsObject:@""]){
        
        [_asScroll setHidden:YES];
        [self cardSetupWithoutImage];
    }
    else{
        [_asScroll setHidden:NO];
        [self cardSetup];
    }
    
    [self imageSetup];
   
    //_imagesName =[[NSArray alloc]init];
      images= [[NSMutableArray alloc] init];
    
 //   NSLog(@"Images array is %@",_imagesName);
    
   //scrollView=[[ScrollViewAdd alloc]init];
   
       if (_imagesName == nil || [_imagesName count] == 0 || [_imagesName containsObject:@""]){
        _imagesName =nil;
       //[scrollView reloadInputViews];
          [scrollView setBackgroundColor:[UIColor redColor]];
        [_asScroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    else{
        [scrollView setBackgroundColor:[UIColor clearColor]];
         [scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
     [self scrollersetup];
    }
       }
-(void)scrollersetup{
    
    
    
//    if([imagesName count]==0){
//        [scrollView removeFromSuperview];
//    }
//    else{
//        
    

   
    int scrollWidth = 320;
    
    scrollView.contentSize = CGSizeMake(scrollWidth,80);
    
    
    
    int xOffset = 2;
    
    int imageTag = 555;
    
   // NSLog(@"_imagesName.post_image_list count is %lu",(unsigned long)[_imagesName count]);
    for(int index=0; index < [_imagesName count]; index++)
    {
        UIImageView *img = [[UIImageView alloc] init];
        
        img.tag = imageTag + index;

        if([_imagesName count]==1){
           img.frame = CGRectMake(xOffset, 0,  scrollView.frame.size.width, scrollView.frame.size.height);
            [img setContentMode:UIViewContentModeScaleAspectFit];
        }
        
        else{
            img.frame = CGRectMake(xOffset, 0, 200, scrollView.frame.size.height);
            [img setContentMode:UIViewContentModeScaleToFill];
        }
        
     
        
        
       // NSLog(@"images array for each row is %@ ,%@",_imagesName,_wallid);
        postImagesfeed *feed=[_imagesName objectAtIndex:index];
        NSString *imageUrl;
        NSURL *url2;
        //for (feed in _imagesName) {
       NSString *str= [KBaseUrlPostListImages stringByAppendingString:@"/small/"];
            imageUrl=[str stringByAppendingString:feed.postimg_image];
            url2 = [NSURL URLWithString:imageUrl];
       
    
       
      
        // download the image asynchronously
        SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
        [downloader downloadImageWithURL:url2
                                 options:0
                                progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                    // progression tracking code
                                }
                               completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                   if (image && finished) {
                                       // do something with image
                                       img.image = image;
                                   }
                               }];
        
        [images insertObject:img atIndex:index];

            scrollView.contentSize = CGSizeMake(scrollWidth+xOffset,110);
            
        
        [scrollView addSubview:[images objectAtIndex:index]];
       
        
       
        [_asScroll addSubview:scrollView];
        if([_imagesName count]==1){
            xOffset += scrollView.frame.size.width;
        }
        
        else{
            xOffset += 210;
        }
         }
         //  }
    
    
    
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
    
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch * touch = [[event allTouches] anyObject];
    
    for(int index=0;index<[images count];index++)
    {
        UIImageView *imgView = [images objectAtIndex:index];
        NSLog(@"x=%f,y=%f,width =%f,height=%f",imgView.frame.origin.x,imgView.frame.origin.y,imgView.frame.size.width,imgView.frame.size.height);
        NSLog(@"x= %f,y=%f",[touch locationInView:scrollView].x,[touch locationInView:scrollView].y) ;
        
        if(CGRectContainsPoint([imgView frame], [touch locationInView:scrollView]))
        {
            [self ShowDetailView:imgView];
            break;
        }
    }
}

-(void)ShowDetailView:(UIImageView *)imgView
{
    int index = imgView.tag - 555;
    NSLog(@"hii === %d", index);
    
     // NSLog(@"Selected index value  %ld",(long)index);
   
    [self myMethodToDoStuff:index ];
    // NSLog(@"Selected index value  %ld",(long)index);
    
}
- (void) myMethodToDoStuff:(NSInteger)imageIndex {
    [self.delegate myClassDelegateMethod:imageIndex :_selectedCell]; //this will call the method implemented in your other class
}



-(void)cardSetup
{
     _postView.translatesAutoresizingMaskIntoConstraints = YES;
    _headerLbl.translatesAutoresizingMaskIntoConstraints = YES;
     imageView.translatesAutoresizingMaskIntoConstraints = YES;
    _dateLbl.translatesAutoresizingMaskIntoConstraints = YES;
    _updatestatusLbl.translatesAutoresizingMaskIntoConstraints = YES;
    
    _descLbl.translatesAutoresizingMaskIntoConstraints = YES;
    
    _asScroll.translatesAutoresizingMaskIntoConstraints = YES;
    _likeLbl.translatesAutoresizingMaskIntoConstraints = YES;
    
    _likeBtn.translatesAutoresizingMaskIntoConstraints = YES;

    _commentLbl.translatesAutoresizingMaskIntoConstraints = YES;
    
    _commentBtn.translatesAutoresizingMaskIntoConstraints = YES;
    scrollView.translatesAutoresizingMaskIntoConstraints = YES;
    [_postView setFrame:CGRectMake(10, 5, self.frame.size.width-20  , 450)];
    [_postView setAlpha:1];
    _postView.layer.masksToBounds = NO;
    _postView.layer.cornerRadius = 1; // if you like rounded corners
    _postView.layer.shadowOffset = CGSizeMake(-.2f, .2f); //%%% this shadow will hang slightly down and to the right
    _postView.layer.shadowRadius = 1; //%%% I prefer thinner, subtler shadows, but you can play with this
    _postView.layer.shadowOpacity = 0.2; //%%% same thing with this, subtle is better for me
    
    //%%% This is a little hard to explain, but basically, it lowers the performance required to build shadows.  If you don't use this, it will lag
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:_postView.bounds];
    _postView.layer.shadowPath = path.CGPath;
    
    self.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]; //%%% I prefer choosing colors programmatically than on the storyboard
    [_headerLbl setFrame:CGRectMake(imageView.frame.origin.x+ imageView.frame.size.width+10, 20, _postView.frame.size.width-120, 20)];
   // [_headerLbl setBackgroundColor:[UIColor redColor]];
    
    [_dateLbl setFrame:CGRectMake(_headerLbl.frame.origin.x, _headerLbl.frame.origin.y+_headerLbl.frame.size.height+1, _postView.frame.size.width-120, 20)];
   // [_dateLbl setBackgroundColor:[UIColor greenColor]];
    
    [_updatestatusLbl setFrame:CGRectMake(imageView.frame.origin.x+ imageView.frame.size.width+10, _dateLbl.frame.origin.y+_dateLbl.frame.size.height+1, _postView.frame.size.width-120, 20)];
   // [_updatestatusLbl setBackgroundColor:[UIColor orangeColor]];
   
    [_descLbl setFrame:CGRectMake(imageView.frame.origin.x, _updatestatusLbl.frame.origin.y+_updatestatusLbl.frame.size.height+10, _postView.frame.size.width-20, 50)];
  //  [_descLbl setBackgroundColor:[UIColor redColor]];
    
    
    
    [_asScroll setFrame:CGRectMake(imageView.frame.origin.x, _descLbl.frame.origin.y+_descLbl.frame.size.height+10, _postView.frame.size.width-40, 250)];
    //[_asScroll setBackgroundColor:[UIColor grayColor]];
   [ scrollView setFrame:CGRectMake(0,0, _asScroll.frame.size.width, _asScroll.frame.size.height)];
    

    
    [_likeLbl setFrame:CGRectMake(imageView.frame.origin.x, _asScroll.frame.origin.y+_asScroll.frame.size.height+15, 80, 30)];
    [_likeLbl sizeToFit];
 //  [_likeLbl setBackgroundColor:[UIColor greenColor]];
    
    [_likeBtn setFrame:CGRectMake(_likeLbl.frame.origin.x+_likeLbl.frame.size.width+1, _asScroll.frame.origin.y+_asScroll.frame.size.height+10, 40, 20)];
 //   [_likeBtn setBackgroundColor:[UIColor greenColor]];
    
    [_likeBtn.titleLabel sizeToFit];
   
    
    [_commentLbl setFrame:CGRectMake(_likeBtn.frame.origin.x+_likeBtn.frame.size.width+5, _asScroll.frame.origin.y+_asScroll.frame.size.height+15, 80, 30)];
    [_commentLbl sizeToFit];
 //   [_commentLbl setBackgroundColor:[UIColor greenColor]];
    [_commentBtn setFrame:CGRectMake(_commentLbl.frame.origin.x+_commentLbl.frame.size.width+1, _asScroll.frame.origin.y+_asScroll.frame.size.height+10, 70, 20)];
  // [_commentBtn setBackgroundColor:[UIColor greenColor]];



}


-(void)cardSetupWithoutImage
{
     imageView.translatesAutoresizingMaskIntoConstraints = YES;
    _postView.translatesAutoresizingMaskIntoConstraints = YES;
    _headerLbl.translatesAutoresizingMaskIntoConstraints = YES;
    
    _dateLbl.translatesAutoresizingMaskIntoConstraints = YES;
    _updatestatusLbl.translatesAutoresizingMaskIntoConstraints = YES;
    
    _descLbl.translatesAutoresizingMaskIntoConstraints = YES;
    
    _asScroll.translatesAutoresizingMaskIntoConstraints = YES;
    _likeLbl.translatesAutoresizingMaskIntoConstraints = YES;
    
    _likeBtn.translatesAutoresizingMaskIntoConstraints = YES;
    
    _commentLbl.translatesAutoresizingMaskIntoConstraints = YES;
    
    _commentBtn.translatesAutoresizingMaskIntoConstraints = YES;
    scrollView.translatesAutoresizingMaskIntoConstraints = YES;
    [_postView setFrame:CGRectMake(10, 5, self.frame.size.width-20  , 200)];
    [_postView setAlpha:1];
    _postView.layer.masksToBounds = NO;
    _postView.layer.cornerRadius = 1; // if you like rounded corners
    _postView.layer.shadowOffset = CGSizeMake(-.2f, .2f); //%%% this shadow will hang slightly down and to the right
    _postView.layer.shadowRadius = 1; //%%% I prefer thinner, subtler shadows, but you can play with this
    _postView.layer.shadowOpacity = 0.2; //%%% same thing with this, subtle is better for me
    
    //%%% This is a little hard to explain, but basically, it lowers the performance required to build shadows.  If you don't use this, it will lag
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:_postView.bounds];
    _postView.layer.shadowPath = path.CGPath;
    
    self.backgroundColor = [UIColor colorWithRed:.9 green:.9 blue:.9 alpha:1]; //%%% I prefer choosing colors programmatically than on the storyboard
    [_headerLbl setFrame:CGRectMake(imageView.frame.origin.x+ imageView.frame.size.width+10, 20, _postView.frame.size.width-120, 20)];
    // [_headerLbl setBackgroundColor:[UIColor redColor]];
    
    [_dateLbl setFrame:CGRectMake(_headerLbl.frame.origin.x, _headerLbl.frame.origin.y+_headerLbl.frame.size.height+1, _postView.frame.size.width-120, 20)];
    // [_dateLbl setBackgroundColor:[UIColor greenColor]];
    
    [_updatestatusLbl setFrame:CGRectMake(imageView.frame.origin.x+ imageView.frame.size.width+10, _dateLbl.frame.origin.y+_dateLbl.frame.size.height+1, _postView.frame.size.width-120, 20)];
    // [_updatestatusLbl setBackgroundColor:[UIColor orangeColor]];
    
    [_descLbl setFrame:CGRectMake(imageView.frame.origin.x, _updatestatusLbl.frame.origin.y+_updatestatusLbl.frame.size.height+10, _postView.frame.size.width-20, 50)];
    //  [_descLbl setBackgroundColor:[UIColor redColor]];
    
    
    
    
    
    [_likeLbl setFrame:CGRectMake(imageView.frame.origin.x, _descLbl.frame.origin.y+_descLbl.frame.size.height+15, 80, 30)];
    [_likeLbl sizeToFit];
    //  [_likeLbl setBackgroundColor:[UIColor greenColor]];
    
    [_likeBtn setFrame:CGRectMake(_likeLbl.frame.origin.x+_likeLbl.frame.size.width+1, _descLbl.frame.origin.y+_descLbl.frame.size.height+10, 40, 20)];
    //   [_likeBtn setBackgroundColor:[UIColor greenColor]];
    
    [_likeBtn.titleLabel sizeToFit];
    
    
    [_commentLbl setFrame:CGRectMake(_likeBtn.frame.origin.x+_likeBtn.frame.size.width+5, _descLbl.frame.origin.y+_descLbl.frame.size.height+15, 80, 30)];
    [_commentLbl sizeToFit];
    //   [_commentLbl setBackgroundColor:[UIColor greenColor]];
    [_commentBtn setFrame:CGRectMake(_commentLbl.frame.origin.x+_commentLbl.frame.size.width+1, _descLbl.frame.origin.y+_descLbl.frame.size.height+10, 70, 20)];
    // [_commentBtn setBackgroundColor:[UIColor greenColor]];
    
    
    
}


-(void)imageSetup
{
    imageView.layer.cornerRadius = imageView.frame.size.width/2;
    imageView.clipsToBounds = YES;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.backgroundColor = [UIColor whiteColor];
    imageView.autoresizingMask=UIViewAutoresizingNone;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
