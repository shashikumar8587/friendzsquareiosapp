//
//  postImagesfeed.h
//  friendsquare
//
//  Created by magnon on 14/01/16.
//  Copyright © 2016 magnon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
//#import "postImagesfeed.h"

@protocol postImagesfeed
@end

@interface postImagesfeed : JSONModel
@property (strong, nonatomic) NSString* postimg_id;
@property (strong, nonatomic) NSString* postimg_u_id;
@property (strong, nonatomic) NSString* postimg_wall_id;
@property (strong, nonatomic) NSString* postimg_image;
@property (strong, nonatomic) NSString* postimg_original_name;
@property (strong, nonatomic) NSString* postimg_added_date;
@property (strong, nonatomic) NSString* postimg_updated_date;
@property (strong, nonatomic) NSString* postimg_status;

@end
//"postimg_id":"2",
//"postimg_u_id":"1",
//"postimg_wall_id":"2",
//"postimg_image":"145275531473ca8366ad48736561726a0f9f0e1503.jpg",
//"postimg_original_name":"Koala.jpg",
//"postimg_added_date":"2016-01-14 01:08:34",
//"postimg_updated_date":null,
//"postimg_status":"1"