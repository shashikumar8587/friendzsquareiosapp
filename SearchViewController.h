//
//  SearchViewController.h
//  Gastronome
//
//  Created by MIPC-52 on 26/03/14.
//  Copyright (c) 2014 Magnon International. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendsModel.h"
@interface SearchViewController : UIViewController<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>{
   
    NSArray *SearchArray;
    FriendsModel *_items;
}
@property (weak, nonatomic) IBOutlet UITableView *connectionTableView;
@property (weak, nonatomic) IBOutlet UILabel *searchResultLbl;
@property (weak, nonatomic) IBOutlet UILabel *resultCount;

@property(strong,nonatomic)NSArray *SearchArray;
@end
