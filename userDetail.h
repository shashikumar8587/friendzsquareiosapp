//
//  userDetail.h
//  friendsquare
//
//  Created by magnon on 23/10/15.
//  Copyright © 2015 magnon. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"

@interface userDetail : JSONModel{
    
}
@property(nonatomic,copy)NSString *total_connection;
@property(nonatomic,copy)NSString *u_display_name;
@property(nonatomic,copy)NSString *u_id;
@property(nonatomic,copy)NSString *u_first_name;
@property(nonatomic,copy)NSString *u_last_name;
@property(nonatomic,copy)NSString *u_email;
@property(nonatomic,copy)NSString *u_gender;
@property(nonatomic,copy)NSString *u_personal_visibility;
@property(nonatomic,copy)NSString *u_marital_status;
@property(nonatomic,copy)NSString *u_profile_photo;
@property(nonatomic,copy)NSString *u_country;
@property(nonatomic,copy)NSString *u_state;
@property(nonatomic,copy)NSString *u_city;
@property(nonatomic,copy)NSString *u_zip_code;
@property(nonatomic,copy)NSString *u_phone_number;
@property(nonatomic,copy)NSString *u_qualification_visibility;
@property(nonatomic,copy)NSString *u_education_level;
@property(nonatomic,copy)NSString *u_college_specialization;
@property(nonatomic,copy)NSString *u_graduate_year;
@property(nonatomic,copy)NSString *u_study_status;
@property(nonatomic,copy)NSString *u_city_establishment;
@property(nonatomic,copy)NSString *u_country_establishment;
@property(nonatomic,copy)NSString *u_education_establishment;
@property(nonatomic,copy)NSString *u_professional_visibility;
@property(nonatomic,copy)NSString *u_current_profession;
@property(nonatomic,copy)NSString *u_sub_profession;
@property(nonatomic,copy)NSString *u_current_position;
@property(nonatomic,copy)NSString *u_doc;

// save user preference for automatic login
- (void) savePreference;

// code to set user data
- (void) setUserData:(NSDictionary*) dict;

- (void) destroyPreference;


-(void)saveEditProfile:(NSDictionary*) dict;
+ (userDetail *)sharedCenter;
@end
